package com.zenya.sgcraftaddon.events;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.storage.CItemStorage;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

public class CItemHandlers {
    private static CItemHandlers CItemHandlers;
    private static LoreUtils loreUtils = LoreUtils.getInstance();
    private static CItemStorage cItemStorage = CItemStorage.getInstance();

    public void handleEvent(ItemStack item, Event event, Trigger trigger) {
        String key = loreUtils.getKey(item, "&8ID");
        if(key == null) return;

        CItem cItem = cItemStorage.getCItem(key);
        if(cItem == null) return;

        cItem.onTrigger(event, trigger);
    }

    public void handleEvent(Entity entity, Event event, Trigger trigger) {
        String key = entity.getCustomName();
        if(key == null) return;

        CItem cItem = cItemStorage.getCItem(key);
        if(cItem == null) return;

        cItem.onTrigger(event, trigger);
    }

    public static CItemHandlers getInstance() {
        if(CItemHandlers == null) {
            CItemHandlers = new CItemHandlers();
        }
        return CItemHandlers;
    }
}
