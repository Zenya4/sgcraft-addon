package com.zenya.sgcraftaddon.events;

import com.zenya.sgcraftaddon.inventory.ToolBoxGUI;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.ClaimPvPStorage;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.GriefPreventionHook;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import me.ryanhamshire.GriefPrevention.Claim;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.inventory.CartographyInventory;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Listeners implements Listener {
    private static LoreUtils loreUtils = LoreUtils.getInstance();
    //private static DiscordHook discordHook;
    private static ClaimPvPStorage claimPvPStorage = ClaimPvPStorage.getInstance();
    private HashMap<Player, Long> warns = new HashMap<Player, Long>();

    /*static {
        try {
            discordHook = DiscordHook.getInstance();
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }*/

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    SGCraftAddon.sqLiteManager.initQuestPoints(event.getPlayer(), 0);
                } catch(Exception e) {
                    // Silence errors
                }
            }
        }.runTaskAsynchronously(SGCraftAddon.instance);
    }

    @EventHandler
    public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        if(event.getMessage().toUpperCase().equals("F") && !(event.isCancelled())) {
            ChatUtils.sendBroadcast("&8> &f" + event.getPlayer().getName() + " &7paid respect.");
            event.setCancelled(true);
        }
        if(event.getMessage().toUpperCase().equals("X") && !(event.isCancelled())) {
            ChatUtils.sendBroadcast("&8> &f" + event.getPlayer().getName() + " &7doubts.");
            event.setCancelled(true);
        }
        if(event.getMessage().toUpperCase().equals("Y") && !(event.isCancelled())) {
            ChatUtils.sendBroadcast("&8> &f" + event.getPlayer().getName() + " &7has shamed.");
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent event) {
        ToolBoxGUI toolBoxGUI = ToolBoxGUI.getInstance();

        if(event.getClickedInventory() != null && event.getClickedInventory().getHolder() != null && event.getClickedInventory().getHolder().equals(toolBoxGUI)) {

            Player player = (Player) event.getWhoClicked();
            ItemStack clickedItem = event.getCurrentItem();
            if (clickedItem == null || clickedItem.getType() == Material.AIR) {
                return;
            }
            toolBoxGUI.initItems();
            if (event.getAction().equals(InventoryAction.PICKUP_ALL)) {
                player.getInventory().addItem(clickedItem);
                ChatUtils.sendMessage(player, "&6You have been given a " + clickedItem.getItemMeta().getDisplayName());
            }
            event.setCancelled(true);
        }


        if(event.getClickedInventory() != null && (event.getClickedInventory() instanceof CartographyInventory || event.getClickedInventory() instanceof CraftingInventory)) {
            if(event.getClickedInventory().getContents() == null || event.getClickedInventory().getContents().length == 0) {
                return;
            }

            for(ItemStack item : event.getClickedInventory().getContents()) {
                if(loreUtils.getKey(item, "&9Locked by") instanceof String) {
                    event.getWhoClicked().closeInventory();
                    ChatUtils.sendMessage((Player) event.getWhoClicked(), "&cYou cannot clone this locked map (/maplock)");
                    event.setCancelled(true);
                }
            }
        }
    }

    /*@EventHandler
    public void onPlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        if(!(player.hasPermission("discordhook.use")) && !(player.hasPermission("sgcraftaddon.admin"))) {
            return;
        }
        discordHook.parseCommand(event.getPlayer(), event.getMessage());
    }*/

    @EventHandler
    public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent event) {
        if(!(event.getRightClicked() instanceof Villager)) {
            return;
        }
        Villager villager = (Villager) event.getRightClicked();

        if(!villager.getProfession().equals(Villager.Profession.CARTOGRAPHER)) {
            return;
        }

        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("villagertrade.notify");
        //ChatUtils.sendProtectedBroadcast(permissions, "&4" + event.getPlayer().getName() + " &cis trading with a cartographer at &f" + villager.getLocation().getX() + ", " + villager.getLocation().getY() + ", " + villager.getLocation().getZ());
    }

    @EventHandler
    public void onPlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String cmd = event.getMessage();

        if(!player.hasPermission("sgcraftaddon.admin")) return;
        if(!cmd.contains("%all%")) return;

        for(Player p : Bukkit.getOnlinePlayers()) {
            player.chat(cmd.replaceAll("%all%", p.getName()));
        }
    }

    @EventHandler
    public void onServerCommandEvent(ServerCommandEvent event) {
        CommandSender sender = event.getSender();
        String cmd = event.getCommand();

        if(!cmd.contains("%all%")) return;

        for(Player p : Bukkit.getOnlinePlayers()) {
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd.replaceAll("%all%", p.getName()));
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPVP(EntityDamageByEntityEvent event) {
        Entity en = event.getDamager();
        EntityType entype = en.getType();
        switch (entype.toString()) {
            case "LINGERING_POTION":
            case "SPECTRAL_ARROW":
            case "TIPPED_ARROW":
            case "SPLASH_POTION":
            case "ARROW":
            case "PLAYER":
            case "FIREWORK":
            case "FIREBALL":
            case "SMALL_FIREBALL":
            case "SNOWBALL":
            case "EGG":
            case "TRIDENT":
                break;
            default:
                return;
        }
        Entity target = event.getEntity();
        if (!(target instanceof Player)) {
            return;
        }
        Player p = null;
        if (en instanceof Player) {
            p = (Player) en;
        }
        if (en instanceof Projectile) {
            ProjectileSource pr = ((Projectile) en).getShooter();
            if (!(pr instanceof Player)) {
                return;
            }
            p = (Player) pr;
        }
        Player ptarget = (Player) event.getEntity();
        if (en instanceof Firework) {
            Claim clai = GriefPreventionHook.getClaimAtLocation(ptarget, ptarget.getLocation());
            if (clai == null) {
                return;
            }
            Boolean ispvpa = claimPvPStorage.isPvpEnabled(clai);
            if (ispvpa == null) {
                ispvpa = false;
            }
            if (ispvpa == false) {
                event.setCancelled(true);
            }
            return;
        }
        Claim claim = GriefPreventionHook.getClaimAtLocation(ptarget, ptarget.getLocation());
        Claim aclaim = GriefPreventionHook.getClaimAtLocation(p, p.getLocation());
        if (claim == null && aclaim == null) {
            return;
        }
        Boolean ispvpen = claimPvPStorage.isPvpEnabled(claim);
        if (ispvpen == null) {
            claimPvPStorage.setPVP(claim, false);
            ispvpen = false;
        }
        if (claim != aclaim) {
            if (claimPvPStorage.isPvpEnabled(aclaim)) {
                return;
            }
            event.setCancelled(true);
            if (en instanceof Projectile) { en.setFireTicks(0);
                ptarget.setFireTicks(0);
            }
            if (warns.get(p) == null) {
                warns.put(p, 0L);
            }
            if (System.currentTimeMillis() - warns.get(p) < 3500L) {
                ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here!");
            }
            return;
        }
        if (ispvpen == true) {
            return;
        }

        if (en instanceof Trident) {
            if (ispvpen == false) {
                event.setCancelled(true);
                if (warns.get(p) == null) {
                    warns.put(p, 0L);
                }
                if (System.currentTimeMillis() - warns.get(p) < 3500L) {
                    return;
                }
                if (claim.getOwnerName() == p.getName()) {
                    warns.put(p, System.currentTimeMillis());
                    ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here! &o(/claimpvp to re-enable)");
                    return;
                }
                warns.put(p, System.currentTimeMillis());
                ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here!");
                return;
            }
            else {
                event.setCancelled(true);
                if (warns.get(p) == null) {
                    warns.put(p, 0L);
                }
                if (System.currentTimeMillis() - warns.get(p) < 3500L) {
                    return;
                }
                if (claim.getOwnerName() == p.getName()) {
                    warns.put(p, System.currentTimeMillis());
                    ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here! &o(/claimpvp to re-enable)");
                    return;
                }
                warns.put(p, System.currentTimeMillis());
                ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here!");
                return;
            }
        }
        if (en instanceof Player) {
            if (ispvpen == false) {
                event.setCancelled(true);
                if (warns.get(p) == null) {
                    warns.put(p, 0L);
                }
                if (System.currentTimeMillis() - warns.get(p) < 3500L) {
                    return;
                }
                if (claim.getOwnerName() == p.getName()) {
                    warns.put(p, System.currentTimeMillis());
                    ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here! &o(/claimpvp to re-enable)");
                    return;
                }
                warns.put(p, System.currentTimeMillis());
                ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here!");
                return;
            }
            else {
                event.setCancelled(true);
                if (warns.get(p) == null) {
                    warns.put(p, 0L);
                }
                if (System.currentTimeMillis() - warns.get(p) < 3500L) {
                    return;
                }
                if (claim.getOwnerName() == p.getName()) {
                    warns.put(p, System.currentTimeMillis());
                    ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here! &o(/claimpvp to re-enable)");
                    return;
                }
                warns.put(p, System.currentTimeMillis());
                ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here!");
                return;
            }
        }
        if (en instanceof Projectile) {
            Projectile fire = (Projectile) en;
            ProjectileSource src = fire.getShooter();
            if (!(src instanceof Player)) {
                return;
            }
            if (ispvpen == false) {
                fire.setFireTicks(0);
                ptarget.setFireTicks(0);
                event.setCancelled(true);
                if (warns.get(p) == null) {
                    warns.put(p, 0L);
                }
                if (System.currentTimeMillis() - warns.get(p) < 3500L) {
                    return;
                }
                if (claim.getOwnerName() == p.getName()) {
                    warns.put(p, System.currentTimeMillis());
                    ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here! &o(/claimpvp to re-enable)");
                    return;
                }
                warns.put(p, System.currentTimeMillis());
                ChatUtils.sendMessage(p, "&c&lOops! &7PVP is disabled here!");
                return;
            }
            return;
        }
        if (en instanceof LightningStrike) {
            LightningStrike light = (LightningStrike) en;
            if (ispvpen == false) {
                light.remove();
                ptarget.setFireTicks(0);
                event.setCancelled(true);
                return;
            }
            return;
        }
    }

}
