package com.zenya.sgcraftaddon.events;

import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.storage.ValueStorage;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class CItemListeners implements Listener {
    private static CItemHandlers cItemHandlers = CItemHandlers.getInstance();
    private static ValueStorage valueStorage = ValueStorage.getInstance();

    @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        if(event.isCancelled()) {
            return;
        }

        if(event.getDamager() instanceof Player) {

            Player damager = (Player) event.getDamager();
            cItemHandlers.handleEvent(damager.getInventory().getItemInMainHand(), event, Trigger.ENTITY_DAMAGE_BY_ENTITY);
        }

        if(event.getDamager() instanceof Arrow) {

            Entity entity = event.getDamager();
            entity.setCustomName("HookshotBow");
            cItemHandlers.handleEvent(entity, event, Trigger.ENTITY_DAMAGE_BY_ENTITY);
        }
    }

    @EventHandler
    public void onProjectileLaunchEvent(ProjectileLaunchEvent event) {
        if(!(event.getEntity().getShooter() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity().getShooter();
        cItemHandlers.handleEvent(player.getInventory().getItemInMainHand(), event, Trigger.SNOWBALL_LAUNCH);

        // Hacky way to insert CEvent
        if(event.getEntity().toString().equals("CraftSnowball") && valueStorage.snowballBoom) {
            event.getEntity().setCustomName("Firecracker");
        }
    }

    @EventHandler
    public void onProjectileHitEvent(ProjectileHitEvent event) {
        Entity entity = event.getEntity();
        if(entity.getCustomName() != null) {
            if(entity.getCustomName().equals("Firecracker") || entity.getCustomName().equals("Egg") || entity.getCustomName().equals("RodOfSouls")) {
                cItemHandlers.handleEvent(entity, event, Trigger.SNOWBALL_HIT);
            }
        }
    }

    @EventHandler
    public void onBlockPlaceEvent(BlockPlaceEvent event) {
        Player player = event.getPlayer();

        cItemHandlers.handleEvent(player.getInventory().getItemInMainHand(), event, Trigger.BLOCK_PLACE);
    }

    @EventHandler
    public void onEntityShootBowEvent(EntityShootBowEvent event) {
        if(!(event.getEntity() instanceof Player)) {
            return;
        }

        cItemHandlers.handleEvent(event.getBow(), event, Trigger.ENTITY_SHOOT_BOW);
    }

    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        cItemHandlers.handleEvent(player.getInventory().getBoots(), event, Trigger.PLAYER_MOVE);
    }

    @EventHandler
    public void onPlayerInteractAtEntityEvent(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();

        cItemHandlers.handleEvent(player.getInventory().getItemInMainHand(), event, Trigger.PLAYER_INTERACT_AT_ENTITY);
    }

    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        cItemHandlers.handleEvent(player.getInventory().getItemInMainHand(), event, Trigger.PLAYER_INTERACT);
        cItemHandlers.handleEvent(player.getInventory().getBoots(), event, Trigger.PLAYER_INTERACT);
    }

    @EventHandler
    public void onPlayerItemConsumeEvent(PlayerItemConsumeEvent event) {
        Player player = event.getPlayer();

        cItemHandlers.handleEvent(player.getInventory().getItemInMainHand(), event, Trigger.CONSUME_ITEM);
    }
}
