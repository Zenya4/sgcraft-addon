package com.zenya.sgcraftaddon.inventory;

import com.zenya.sgcraftaddon.storage.CItemStorage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class ToolBoxGUI implements InventoryHolder {
    private static ToolBoxGUI instance;
    private CItemStorage cItemStorage =  CItemStorage.getInstance();
    private Inventory inv;

    private ToolBoxGUI() {
        inv = Bukkit.createInventory(this, 54, ChatColor.translateAlternateColorCodes('&', "&bZenya\'s Toolbox"));
        initItems();
    }

    @Override
    public Inventory getInventory() {
        return inv;
    }

    public void initItems() {
        inv.clear();

        for(String key : cItemStorage.getKeys()) {
            ItemStack item = cItemStorage.getCItem(key).getItem();
            inv.addItem(item);
        }
    }

    public void openInventory(Player player) {
        initItems();
        player.openInventory(inv);
    }

    public static ToolBoxGUI getInstance() {
        if(instance == null) {
            instance = new ToolBoxGUI();
        }
        return instance;
    }
}
