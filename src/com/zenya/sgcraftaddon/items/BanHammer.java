package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class BanHammer implements CItem {
    LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case ENTITY_DAMAGE_BY_ENTITY: {
                EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
                if(!(e.getDamager() instanceof Player) || !(e.getEntity() instanceof Player)) {
                    return;
                }

                Player banner = (Player) e.getDamager();
                Player target = (Player) e.getEntity();

                for(String permission : getPermissions()) {
                    if(banner.hasPermission(permission)) {

                        banner.getWorld().strikeLightning(target.getLocation());
                        Bukkit.getServer().dispatchCommand((CommandSender) banner, "cmi ban " + target.getName() + " The banhammer has spoken");
                        return;
                    }
                }
                ChatUtils.sendMessage(banner, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "BanHammer";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.ENTITY_DAMAGE_BY_ENTITY);

        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("citem.use.banhammer");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&cBan Hammer");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7Strike down with the mighty powers of an admin"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Left click to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.DIAMOND_AXE;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
