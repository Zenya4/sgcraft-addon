package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class SnowBoots implements CItem {
    LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case PLAYER_MOVE: {
                final PlayerMoveEvent e = (PlayerMoveEvent) event;
                final Player player = e.getPlayer();
                final Block b = e.getTo().getBlock();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        if(b.getType().equals(Material.AIR)) {
                            b.setType(Material.SNOW);
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    if(b.getType().equals(Material.SNOW)) {
                                        player.getWorld().playEffect(e.getTo(), Effect.EXTINGUISH, 60);
                                        b.setType(Material.AIR);
                                    }
                                }
                            }.runTaskLater(SGCraftAddon.instance, 200);
                            return;
                        }
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "SnowBoots";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.PLAYER_MOVE);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.player");
        permissions.add("citem.use.snowboots");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&eSnow Boots");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7Let it snow, let it snow, let it snow"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Walk to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.LEATHER_BOOTS;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
