package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FireworkBattery implements CItem {
    LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case BLOCK_PLACE: {
                BlockPlaceEvent e = (BlockPlaceEvent) event;
                Player player = e.getPlayer();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        Location loc = e.getBlock().getLocation();
                        final Location startLoc = loc.clone();
                        loc.setY(loc.getY()+1);
                        loc.setX(loc.getX()+0.5);
                        loc.setZ(loc.getZ()+0.5);
                        final Location l = loc;
                        final World w = l.getWorld();
                        final Material m = e.getBlock().getType();
                        new BukkitRunnable() {
                            int fireworks = 400;
                            @Override
                            public void run() {
                                if(startLoc.getBlock().getType().equals(m) && fireworks > 0) {
                                    w.createExplosion(l, 0f);
                                    final Firework fw = (Firework) w.spawnEntity(l, EntityType.FIREWORK);
                                    FireworkMeta fm = fw.getFireworkMeta();
                                    Random r = new Random();
                                    int type = r.nextInt(4) + 1;
                                    int colors = r.nextInt(3) + 1;
                                    FireworkEffect.Type ft = null;
                                    Color c1 = null;
                                    Color c2 = null;
                                    switch (type) {
                                        case 1:
                                            ft = FireworkEffect.Type.BALL;
                                            break;
                                        case 2:
                                            ft = FireworkEffect.Type.BALL_LARGE;
                                            break;
                                        case 3:
                                            ft = FireworkEffect.Type.BURST;
                                            break;
                                        case 4:
                                            ft = FireworkEffect.Type.STAR;
                                            break;
                                    }
                                    switch (colors) {
                                        case 1:
                                            c1 = Color.RED;
                                            c2 = Color.ORANGE;
                                            break;
                                        case 2:
                                            c1 = Color.BLUE;
                                            c2 = Color.TEAL;
                                            break;
                                        case 3:
                                            c1 = Color.GREEN;
                                            c2 = Color.LIME;
                                            break;
                                    }
                                    FireworkEffect effect =  FireworkEffect.builder().flicker(true).withColor(c1).withFade(c2).trail(true).with(ft).trail(true).build();
                                    fm.addEffect(effect);
                                    fm.setPower(r.nextInt(2) + 2);
                                    fw.setFireworkMeta(fm);
                                    Vector v = fw.getVelocity();
                                    fw.setVelocity(new Vector(v.getX() + (r.nextDouble() - r.nextDouble())*0.01, v.getY(), v.getZ() + (r.nextDouble() - r.nextDouble())*0.01));

                                    fireworks--;
                                }
                            }
                        }.runTaskTimer(SGCraftAddon.instance, 20l, 10l);
                    }
                    return;
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "FireworkBattery";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.BLOCK_PLACE);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.player");
        permissions.add("citem.use.fireworkbattery");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&6Firework Battery");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7Ohh, fireworks! So pretty..."));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Place down to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.REDSTONE_BLOCK;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
