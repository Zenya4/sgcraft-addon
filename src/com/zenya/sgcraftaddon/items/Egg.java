package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Egg implements CItem {
    private static LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        String[] modes = {"NO_DAMAGE", "DAMAGE", "TNT"};

        switch(trigger) {
            case PLAYER_INTERACT: {
                PlayerInteractEvent e = (PlayerInteractEvent) event;
                Player player = e.getPlayer();
                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {

                        if(e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                            ItemStack item = e.getItem();
                            if(loreUtils.getKey(item, "&8Mode").equals("NO_DAMAGE")) {
                                item = loreUtils.setKey(item, "&8Mode", "DAMAGE");
                            } else if(loreUtils.getKey(item, "&8Mode").equals("DAMAGE")) {
                                item = loreUtils.setKey(item, "&8Mode", "TNT");
                            } else if(loreUtils.getKey(item, "&8Mode").equals("TNT")) {
                                item = loreUtils.setKey(item, "&8Mode", "NO_DAMAGE");
                            }
                        }
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }

            case SNOWBALL_LAUNCH: {
                ProjectileLaunchEvent e = (ProjectileLaunchEvent) event;
                Player player = (Player) e.getEntity().getShooter();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        e.getEntity().setCustomName("Egg");
                        e.getEntity().setMetadata("Mode", new FixedMetadataValue(SGCraftAddon.instance, ChatColor.stripColor(loreUtils.getKey(player.getItemInHand(), "&8Mode"))));
                        e.getEntity().setMetadata("Split", new FixedMetadataValue(SGCraftAddon.instance, 15));
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }

            case SNOWBALL_HIT: {
                ProjectileHitEvent e = (ProjectileHitEvent) event;
                Location loc = e.getEntity().getLocation();
                World world = loc.getWorld();
                MetadataValue mode = e.getEntity().getMetadata("Mode").get(0);
                MetadataValue split = e.getEntity().getMetadata("Split").get(0);

                if(split.asInt() > 0) {
                    spawnEgg(e, 1);

                    switch(mode.asString()) {
                        case "NO_DAMAGE": {
                            spawnEgg(e, 2);
                            break;
                        }

                        case "DAMAGE": {
                            e.getHitEntity().setFireTicks(60);
                            if(e.getHitEntity() instanceof Player) {
                                ((Player) e.getHitEntity()).damage(2);
                            }
                        }

                        case "TNT": {
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    world.spawnEntity(loc, EntityType.PRIMED_TNT);
                                }
                            }.runTaskLater(SGCraftAddon.instance, 5);
                            break;
                        }
                    }
                }
            }
        }

    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "Egg";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.PLAYER_INTERACT);
        triggers.add(Trigger.SNOWBALL_LAUNCH);
        triggers.add(Trigger.SNOWBALL_HIT);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("sgcraftaddon.citem.egg");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&aEgg");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7Just a normal egg, nothing to see here"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Left click to change mode"));
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to throw"));
        desc.add("");
        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.EGG;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8Mode", "NO_DAMAGE");
        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }

    public void spawnEgg(ProjectileHitEvent e, int delay) {
        Random rObj = new Random();
        Location loc = e.getEntity().getLocation();
        World world = loc.getWorld();
        MetadataValue mode = e.getEntity().getMetadata("Mode").get(0);
        MetadataValue split = e.getEntity().getMetadata("Split").get(0);

        new BukkitRunnable() {
            @Override
            public void run() {
                org.bukkit.entity.Egg begg = (org.bukkit.entity.Egg) world.spawnEntity(loc, EntityType.EGG);
                double x = begg.getLocation().getX();
                double y = begg.getLocation().getY();
                double z = begg.getLocation().getZ();

                begg.setCustomName(e.getEntity().getCustomName());
                begg.setMetadata("Mode", mode);
                begg.setMetadata("Split", new FixedMetadataValue(SGCraftAddon.instance, split.asInt()-1));

                begg.setBounce(true);

                if(!world.getBlockAt(new Location(world, x, y+1, z)).getType().equals(Material.AIR)) {
                    begg.setVelocity(new Vector(rObj.nextFloat()-rObj.nextFloat(), -1, rObj.nextFloat()-rObj.nextFloat()));
                } else {
                    begg.setVelocity(new Vector(rObj.nextFloat()-rObj.nextFloat(), 1, rObj.nextFloat()-rObj.nextFloat()));
                }
            }
        }.runTaskLater(SGCraftAddon.instance, delay);
    }
}

