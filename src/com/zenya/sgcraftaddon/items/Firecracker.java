package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.storage.ValueStorage;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Firecracker implements CItem {
    private static LoreUtils loreUtils = LoreUtils.getInstance();
    private static ValueStorage valueStorage = ValueStorage.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case SNOWBALL_LAUNCH: {
                ProjectileLaunchEvent e = (ProjectileLaunchEvent) event;
                Player player = (Player) e.getEntity().getShooter();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission) || valueStorage.snowballBoom) {
                        e.getEntity().setCustomName("Firecracker");
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }

            case SNOWBALL_HIT: {
                ProjectileHitEvent e = (ProjectileHitEvent) event;
                Location loc = e.getEntity().getLocation();
                World world = loc.getWorld();
                Firework firework = (Firework) world.spawnEntity(loc, EntityType.FIREWORK);
                FireworkMeta fireworkMeta = firework.getFireworkMeta();

                FireworkEffect.Type type = null;
                Random randObj = new Random();

                int _type = randObj.nextInt(3) + 1;
                int colors = randObj.nextInt(3) + 1;
                Color c1 = null;
                Color c2 = null;

                switch (_type) {
                    case 1:
                        type = FireworkEffect.Type.BALL;
                        break;
                    case 2:
                        type = FireworkEffect.Type.BALL_LARGE;
                        break;
                    case 3:
                        type = FireworkEffect.Type.BURST;
                        break;
                }

                switch (colors) {
                    case 1:
                        c1 = Color.RED;
                        c2 = Color.ORANGE;
                        break;
                    case 2:
                        c1 = Color.BLUE;
                        c2 = Color.TEAL;
                        break;
                    case 3:
                        c1 = Color.GREEN;
                        c2 = Color.LIME;
                        break;
                }

                FireworkEffect effect = FireworkEffect.builder().flicker(false).withColor(c1).withFade(c2).trail(true).with(type).trail(false).build();
                world.createExplosion(loc, 0f);
                fireworkMeta.addEffect(effect);
                firework.setFireworkMeta(fireworkMeta);

                // Boom
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        firework.detonate();
                    }
                }.runTaskLater(SGCraftAddon.instance, 1l);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "Firecracker";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.SNOWBALL_LAUNCH);
        triggers.add(Trigger.SNOWBALL_HIT);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.player");
        permissions.add("citem.use.firecracker");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&6Firecracker");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7Who added gunpowder in my snowball?"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.SNOWBALL;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
