package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class HookshotBow implements CItem {
    LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case PLAYER_INTERACT: {
                String	PushLine	= ChatColor.DARK_GRAY + "Mode: Push";
                String	PullLine	= ChatColor.DARK_GRAY + "Mode: Pull";

                PlayerInteractEvent e = (PlayerInteractEvent) event;

                if(e.getAction().equals(Action.LEFT_CLICK_AIR) || ((PlayerInteractEvent) event).getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                    Player player = ((PlayerInteractEvent) event).getPlayer();
                    ItemStack item = player.getItemInHand();
                    ItemMeta im = item.getItemMeta();
                    List<String> lore = new ArrayList<String>();
                    if(im.hasLore())
                        lore =  im.getLore();
                    player.getWorld().playEffect(player.getLocation(), Effect.CLICK1, 10);
                    String newMode = ChatColor.DARK_GRAY + "Push";
                    if(lore.contains(PushLine)) {
                        lore.remove(PushLine);
                        lore.add(PullLine);
                        newMode = ChatColor.DARK_GRAY + "Pull";
                    } else  {
                        if(lore.size() == 0)
                            lore.add(PushLine);
                        else
                            lore.remove(PullLine);
                        lore.add(PushLine);
                    }

                    im.setLore(lore);
                    item.setItemMeta(im);
                    player.sendMessage(ChatColor.GREEN + "Hookshot Mode: " + newMode);
                }
                break;
            }

            case ENTITY_SHOOT_BOW: {
                EntityShootBowEvent e = (EntityShootBowEvent) event;
                Player player = (Player) e.getEntity();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        ItemMeta im = e.getBow().getItemMeta();
                        if(!im.hasLore())
                            return;
                        List<String> lore = im.getLore();
                        e.getProjectile().setMetadata("ce.hookshotbow", new FixedMetadataValue(SGCraftAddon.instance, ChatColor.stripColor(lore.get(lore.size() - 1)).split(": ")[1]));
                    }
                    return;
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }

            case ENTITY_DAMAGE_BY_ENTITY: {
                EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
                Projectile pr = ((Projectile) e.getDamager());
                if(!(pr.getShooter() instanceof Player)) {
                    return;
                }
                Player player = (Player) pr.getShooter();
                Entity target = e.getEntity();

                Location targetLocation = target.getLocation();
                Location playerLocation = player.getLocation();

                Vector vec = targetLocation.subtract(playerLocation).toVector();

                if(pr.getMetadata("ce.hookshotbow") == null || pr.getMetadata("ce.hookshotbow").size() == 0) return;

                // TODO: REWRITE
                //double distance = targetLocation.distance(playerLocation);
                if(pr.getMetadata("ce.hookshotbow").get(0).asString().equalsIgnoreCase("Push")) {
                    vec.normalize();
                    vec.multiply(10);
                    player.setFallDistance(-10);
                    player.setVelocity(vec);
                } else {
                    vec = player.getLocation().subtract(target.getLocation()).toVector();
                    vec.normalize();
                    vec.multiply(5);
                    target.setFallDistance(-10);
                    target.setVelocity(vec);
                    }
                break;
                }
            }
        }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "HookshotBow";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.PLAYER_INTERACT);
        triggers.add(Trigger.ENTITY_SHOOT_BOW);
        triggers.add(Trigger.ENTITY_DAMAGE_BY_ENTITY);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("citem.use.hookshotbow");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&bHookshot Bow");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7Now everyone can become a slime champion"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Left click to toggle mode"));
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to use"));
        desc.add("");
        desc.add(ChatColor.DARK_GRAY + "Mode: Pull");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.BOW;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
