package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HiveWand implements CItem {
    LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case ENTITY_DAMAGE_BY_ENTITY: {
                EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
                if(!(e.getDamager() instanceof Player)) {
                    return;
                }

                Player attacker = (Player) e.getDamager();
                LivingEntity target = (LivingEntity) e.getEntity();

                Random randObj = new Random();
                Integer rInt = randObj.nextInt(4) + 2;

                for(String permission : getPermissions()) {
                    if(attacker.hasPermission(permission)) {
                        for(int i=0; i<rInt; i++) {
                            Bee bee = (Bee) attacker.getWorld().spawnEntity(target.getLocation(), EntityType.BEE);
                            bee.setTarget(target);
                            bee.setAnger(300);

                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    bee.remove();
                                }
                            }.runTaskLater(SGCraftAddon.instance, 300L);
                        }
                        return;
                    }
                }
                ChatUtils.sendMessage(attacker, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "HiveWand";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.ENTITY_DAMAGE_BY_ENTITY);

        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("citem.use.hivewand");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&cHive Wand");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7BEES BEES BEES, BEES BEES BEES BEES"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Left click to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.HONEYCOMB;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
