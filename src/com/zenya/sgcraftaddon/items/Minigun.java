package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class Minigun implements CItem {
    private LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case ENTITY_SHOOT_BOW: {
                EntityShootBowEvent e = (EntityShootBowEvent) event;
                Player player = (Player) e.getEntity();
                Arrow oldArrow = (Arrow) e.getProjectile();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        e.setCancelled(true);

                        String meta = null;
                        if(oldArrow.hasMetadata("ce.bow.enchantment"))
                            meta = oldArrow.getMetadata("ce.bow.enchantment").get(0).asString();


                        final int fireTicks = oldArrow.getFireTicks();
                        final int knockbackStrength = oldArrow.getKnockbackStrength();
                        final boolean critical = oldArrow.isCritical();
                        final String metadata = meta;

                        new BukkitRunnable() {


                            int lArrows = 20;
                            ItemStack last = player.getInventory().getItemInMainHand();

                            @Override
                            public void run() {
                                if (lArrows > 0) {
                                    if(player.getInventory().getItemInMainHand().hasItemMeta() && player.getItemInHand().getItemMeta().equals(last.getItemMeta())) {
                                        if (player.getGameMode().equals(GameMode.CREATIVE) || player.getInventory().contains(Material.ARROW, 1)) {
                                            if (!player.getGameMode().equals(GameMode.CREATIVE)) {
                                                if(last.getDurability() < 380) {

                                                    last.setDurability((short) (last.getDurability() + 1));
                                                    last = player.getItemInHand();

                                                } else {

                                                    ItemStack brokenItem = new ItemStack(Material.AIR);
                                                    player.setItemInHand(brokenItem);
                                                    player.getWorld().playEffect(player.getLocation(), Effect.ZOMBIE_DESTROY_DOOR, 10);
                                                    player.getWorld().playSound(player.getLocation(), "ENTITY_ITEM_BREAK", 0.4f, 0f);
                                                    this.cancel();

                                                }


                                                ItemStack arrows = new ItemStack(Material.ARROW, 1);

                                                if(!(last.containsEnchantment(Enchantment.ARROW_INFINITE))) {
                                                    player.getInventory().removeItem(arrows);
                                                }
                                            }

                                            Arrow arrow = player.launchProjectile(Arrow.class);
                                            arrow.setBounce(false);
                                            arrow.setVelocity(player.getLocation().getDirection().multiply(5));
                                            arrow.setShooter(player);
                                            arrow.setFireTicks(fireTicks); // Set the new arrows on fire if the original one was
                                            arrow.setKnockbackStrength(knockbackStrength);
                                            arrow.setCritical(critical);
                                            arrow.setPickupStatus(oldArrow.getPickupStatus());
                                            if(metadata != null)
                                                arrow.setMetadata("ce.bow.enchantment", new FixedMetadataValue(SGCraftAddon.instance, metadata));
                                            arrow.setMetadata("ce.minigunarrow", new FixedMetadataValue(SGCraftAddon.instance, null));
                                            player.getWorld().playEffect(player.getLocation(),Effect.BOW_FIRE, 20);
                                            lArrows--;
                                            return;

                                        } else {

                                            player.sendMessage(ChatColor.RED + "Out of ammo!");
                                            player.getWorld().playEffect(player.getLocation(), Effect.CLICK2, 80);

                                        }
                                    }
                                }

                                this.cancel();

                            }
                        }.runTaskTimer(SGCraftAddon.instance, 0l, 1);
                    }
                    return;
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "Minigun";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.ENTITY_SHOOT_BOW);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("citem.use.minigun");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&bMinigun");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7Imagine crossbows, but on meth and fetamine"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.CROSSBOW;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
