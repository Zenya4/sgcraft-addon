package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class RocketBoots implements CItem {
    private LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case PLAYER_INTERACT: {
                PlayerInteractEvent e = (PlayerInteractEvent) event;
                Player player = e.getPlayer();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        ItemStack rocketBoots = e.getPlayer().getInventory().getBoots();

                        final ItemMeta im = rocketBoots.getItemMeta();
                        List<String> lore = im.getLore();
                        if(player.isSneaking()) {

                            if (!player.getGameMode().equals(GameMode.CREATIVE)) {

                                int currentDurability 	= rocketBoots.getDurability();
                                int maxDurability 	 	= rocketBoots.getType().getMaxDurability() - 1;

                                if(currentDurability == (maxDurability * 0.75)) {
                                    player.sendMessage(ChatColor.RED + "Fuel at 25%");
                                } else if(currentDurability == (maxDurability * 0.9)) {
                                    player.sendMessage(ChatColor.RED + "Fuel at 10%");
                                } else if(currentDurability == (maxDurability * 0.95)) {
                                    player.sendMessage(ChatColor.RED + "Fuel at 5%");
                                }

                                if(currentDurability == maxDurability) {

                                    im.setLore(lore);
                                    player.sendMessage(ChatColor.GRAY + "Out of Fuel");
                                    player.updateInventory();
                                    player.getWorld().playSound(player.getLocation(), Sound.ENTITY_BAT_TAKEOFF, 0.2f, 0f);
                                    return;
                                } else {
                                    rocketBoots.setDurability((short) (rocketBoots.getDurability() + 1));
                                    player.updateInventory();
                                }
                            }


                            player.setFallDistance(-10);
                            player.setVelocity(player.getLocation().getDirection().setY(0.5));
                            player.getWorld().playEffect(player.getLocation(), Effect.MOBSPAWNER_FLAMES, 10);
                            player.getWorld().playEffect(player.getLocation(), Effect.SMOKE, 40);
                            player.getWorld().playSound(player.getLocation(), Sound.BLOCK_FIRE_AMBIENT, 0.3f, 2f);
                            if(player.getGameMode().equals(GameMode.CREATIVE))
                                return;

                            final Location loc = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY() - 1, player.getLocation().getZ());

                            if(!player.hasMetadata("ce.flightControl")) {

                                player.setMetadata("ce.flightControl", new FixedMetadataValue(SGCraftAddon.instance, null));

                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        if(player.isFlying()) {
                                            player.setFlying(false);
                                        }
                                        if(loc.getBlock().getRelative(0,-1,0).getType().equals(Material.AIR) && !player.getAllowFlight()) {
                                            player.setAllowFlight(true);
                                        } else {
                                            player.setAllowFlight(false);
                                            player.removeMetadata("ce.flightControl", SGCraftAddon.instance);
                                            this.cancel();
                                        }
                                    }
                                }.runTaskTimer(SGCraftAddon.instance, 0l, 10l);
                            }
                        }
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "RocketBoots";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.PLAYER_INTERACT);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("citem.use.rocketboots");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&eRocket Boots");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7I love you 3000 <3"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Sneak and click to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.IRON_BOOTS;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
