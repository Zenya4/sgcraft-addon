package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public class MagicChorus implements CItem {
    LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case CONSUME_ITEM: {
                PlayerItemConsumeEvent e = (PlayerItemConsumeEvent) event;

                Player player = e.getPlayer();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 200, 1));
                    }

                    if(e.getItem().getAmount() > 1) {
                        e.getItem().setAmount(e.getItem().getAmount() - 1);
                    } else {
                        player.getInventory().removeItem(e.getItem());
                    }

                    e.setCancelled(true);
                    return;
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "MagicChorus";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.CONSUME_ITEM);

        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.player");
        permissions.add("citem.use.magicchorus");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&dMagic Chorus");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7I believe I can fly"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Consume to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.CHORUS_FRUIT;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
