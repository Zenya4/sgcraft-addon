package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class LightningRod implements CItem {
    LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case PLAYER_INTERACT: {
                PlayerInteractEvent e = (PlayerInteractEvent) event;
                Player player = e.getPlayer();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        Location loc = player.getTargetBlock(null, 256).getLocation();
                        player.getWorld().strikeLightning(loc);
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "LightningRod";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.PLAYER_INTERACT);

        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("citem.use.lightningrod");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&3Lightning Rod");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7Pulsating with energy - Bzzz..."));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Left click to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.BAMBOO;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
