package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class YeetStick implements CItem {
    LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch (trigger) {
            case PLAYER_INTERACT_AT_ENTITY: {
                PlayerInteractEntityEvent e = (PlayerInteractEntityEvent) event;
                Player player = e.getPlayer();

                for (String permission : getPermissions()) {
                    if (player.hasPermission(permission)) {
                        e.setCancelled(true);
                        final Entity clicked = e.getRightClicked();
                        if (!player.hasMetadata("ce.yeetstick"))
                            if (!clicked.getType().equals(EntityType.PAINTING) && !clicked.getType().equals(EntityType.ITEM_FRAME) && clicked.getPassenger() != player && player.getPassenger() == null) {
                                player.setMetadata("ce.yeetstick", new FixedMetadataValue(SGCraftAddon.instance, false));

                                player.setPassenger(clicked);

                                player.getWorld().playEffect(player.getLocation(), Effect.ZOMBIE_CHEW_IRON_DOOR, 10);

                                new BukkitRunnable() {

                                    @Override
                                    public void run() {
                                        player.getWorld().playEffect(player.getLocation(), Effect.CLICK2, 10);
                                        player.setMetadata("ce.yeetstick", new FixedMetadataValue(SGCraftAddon.instance, true));
                                        this.cancel();
                                    }
                                }.runTaskLater(SGCraftAddon.instance, 20);

                                new BukkitRunnable() {

                                    int GrabTime = 10;
                                    ItemStack current = player.getItemInHand();

                                    @Override
                                    public void run() {
                                        if (current.equals(player.getItemInHand())) {
                                            current = player.getItemInHand();
                                            if (GrabTime > 0) {
                                                if (!player.hasMetadata("ce.yeetstick")) {
                                                    this.cancel();
                                                }
                                                GrabTime--;
                                            } else if (GrabTime <= 0) {
                                                if (player.hasMetadata("ce.yeetstick")) {
                                                    player.getWorld().playEffect(player.getLocation(), Effect.CLICK1, 10);
                                                    player.removeMetadata("ce.yeetstick", SGCraftAddon.instance);
                                                }
                                                clicked.leaveVehicle();
                                                this.cancel();
                                            }
                                        } else {
                                            player.removeMetadata("ce.yeetstick", SGCraftAddon.instance);
                                            this.cancel();
                                        }
                                    }
                                }.runTaskTimer(SGCraftAddon.instance, 0l, 10l);
                                return;
                            }
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
            case PLAYER_INTERACT: {
                PlayerInteractEvent e = (PlayerInteractEvent) event;
                Player player = e.getPlayer();

                for (String permission : getPermissions()) {
                    if (player.hasPermission(permission)) {
                        if (player.hasMetadata("ce.yeetstick") && player.getMetadata("ce.yeetstick").get(0).asBoolean())
                            if (player.getPassenger() != null) {
                                Entity passenger = player.getPassenger();
                                player.getPassenger().leaveVehicle();
                                passenger.setVelocity(player.getLocation().getDirection().multiply(5));
                                player.getWorld().playEffect(player.getLocation(), Effect.ZOMBIE_DESTROY_DOOR, 10);
                                player.removeMetadata("ce.yeetstick", SGCraftAddon.instance);
                            }
                    }
                }
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "YeetStick";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.PLAYER_INTERACT);
        triggers.add(Trigger.PLAYER_INTERACT_AT_ENTITY);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("citem.use.yeetstick");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&cYeet Stick");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7A stick every server needs"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Left click to mount"));
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to launch"));

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.STICK;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
