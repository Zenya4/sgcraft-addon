package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class MagicBlox implements CItem {
    private static LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        BlockPlaceEvent e = (BlockPlaceEvent) event;
        if(!e.isCancelled()) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    e.getBlockPlaced().setType(Material.AIR);
                }
            }.runTaskLater(SGCraftAddon.instance, 100L);
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "MagicBlox";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.BLOCK_PLACE);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("sgcraftaddon.citem.magicblox");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&dMagic Blox");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7What blocks?"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to use"));
        desc.add("");
        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.GLASS;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
