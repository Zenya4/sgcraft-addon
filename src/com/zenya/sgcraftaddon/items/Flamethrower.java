package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class Flamethrower implements CItem {
    LoreUtils loreUtils = LoreUtils.getInstance();

    private List<Location> getLinePlayer(Player player, int length) {
        List<Location> list = new ArrayList<Location>();
        Vector direction = player.getLocation().getDirection();
        Location cLoc = player.getLocation().add(0, 1, 0); //Current location
        for(int amount = length; amount > 0; amount --) {
            list.add(cLoc.add(direction).getBlock().getLocation());
        }
        return list;
    }

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case PLAYER_INTERACT: {
                PlayerInteractEvent e = (PlayerInteractEvent) event;
                Player player = e.getPlayer();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        e.setCancelled(true);

                        if(player.getItemInHand().getDurability() >= 249) {
                            player.getWorld().playEffect(player.getLocation(), Effect.CLICK1, 2);
                            player.sendMessage(ChatColor.GRAY + "Out of Fuel");
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    if(player.getItemInHand().getDurability() == 0) {
                                        player.getWorld().playEffect(player.getLocation(), Effect.CLICK2, 2);
                                        this.cancel();
                                    } else {
                                        //player.getItemInHand().setDurability((short) (player.getItemInHand().getDurability() - 1));
                                    }
                                }
                            }.runTaskTimer(SGCraftAddon.instance, 0l, 2l);

                        } else {
                            final List<Location> list = getLinePlayer(player, 10);
                            for(final Location l: list) {
                                if(l.getBlock().getType().equals(Material.AIR))
                                    l.getBlock().setType(Material.FIRE);
                                l.getWorld().playEffect(l, Effect.SMOKE, 20);
                                final FallingBlock fire = l.getWorld().spawnFallingBlock(l, Material.FIRE, (byte) 0);
                                fire.setDropItem(false);
                                fire.setVelocity(player.getLocation().getDirection());
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        if(fire.isDead()) {
                                            list.add(fire.getLocation());
                                            this.cancel();
                                        } else {
                                            if(fire.getLocation().getBlock().getType().equals(Material.WATER)) {
                                                fire.getWorld().playEffect(fire.getLocation(), Effect.EXTINGUISH, 60);
                                                fire.remove();
                                                this.cancel();
                                            }
                                            for(Entity ent:fire.getNearbyEntities(0, 0, 0)) {
                                                if(ent != player) {
                                                    ent.setFireTicks(100);
                                                }
                                            }
                                        }
                                    }
                                }.runTaskTimer(SGCraftAddon.instance, 0l, 1l);
                            }
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    for(Location ls:list) {
                                        if(ls.getBlock().getType().equals(Material.FIRE)) {
                                            ls.getWorld().playEffect(ls, Effect.EXTINGUISH, 60);
                                            ls.getBlock().setType(Material.AIR);
                                        }
                                    }
                                }
                            }.runTaskLater(SGCraftAddon.instance, 200l);
                            player.getWorld().playEffect(player.getLocation(), Effect.BLAZE_SHOOT, 30);
                            if(!player.getGameMode().equals(GameMode.CREATIVE)) {
                                player.getItemInHand().setDurability((short) (player.getItemInHand().getDurability() + 1));
                            }
                        }
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "Flamethrower";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.PLAYER_INTERACT);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("citem.use.flamethrower");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&aFlamethrower");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7I like to watch the world burn"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.IRON_HOE;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
