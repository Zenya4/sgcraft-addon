package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class RodOfSouls implements CItem {
    private static LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        String[] modes = {"KNOCKBACK", "SMALL_EXPLODE", "LARGE_EXPLODE"};

        switch(trigger) {
            case PLAYER_INTERACT: {
                PlayerInteractEvent e = (PlayerInteractEvent) event;
                Player player = e.getPlayer();
                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {

                        if(e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                            ItemStack item = e.getItem();
                            if(loreUtils.getKey(item, "&8Mode").equals("KNOCKBACK")) {
                                item = loreUtils.setKey(item, "&8Mode", "SMALL_EXPLODE");
                            } else if(loreUtils.getKey(item, "&8Mode").equals("SMALL_EXPLODE")) {
                                item = loreUtils.setKey(item, "&8Mode", "LARGE_EXPLODE");
                            } else if(loreUtils.getKey(item, "&8Mode").equals("LARGE_EXPLODE")) {
                                item = loreUtils.setKey(item, "&8Mode", "KNOCKBACK");
                            }
                    }
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }

            case SNOWBALL_LAUNCH: {
                List<Location> locs = new ArrayList<Location>();

                ProjectileLaunchEvent e = (ProjectileLaunchEvent) event;
                Projectile proj = e.getEntity();
                Player player = (Player) proj.getShooter();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        proj.setCustomName("RodOfSouls");
                        proj.setMetadata("Mode", new FixedMetadataValue(SGCraftAddon.instance, ChatColor.stripColor(loreUtils.getKey(player.getItemInHand(), "&8Mode"))));

                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                if(!proj.isDead()) {
                                    locs.add(proj.getLocation());
                                    for(int i=0; i<locs.size(); i++) {
                                        proj.getWorld().spawnParticle(Particle.FLAME, locs.get(i), 1, 0, 0, 0, 0);
                                    }

                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            if(locs.size() > 1) {
                                                locs.remove(0);
                                            }
                                        }
                                    }.runTaskTimer(SGCraftAddon.instance, 0, 10);
                                } else {
                                    this.cancel();
                                }
                            }
                        }.runTaskTimer(SGCraftAddon.instance, 4, 1);
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }

            case SNOWBALL_HIT: {
                EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
                Entity damager = e.getDamager();

                if(damager instanceof FishHook) {
                    MetadataValue mode = damager.getMetadata("Mode").get(0);
                    switch(mode.asString()) {

                        case "KNOCKBACK": {
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    if(e.getEntity() instanceof Player) {
                                        ((Player) (e.getEntity())).damage(0);
                                    }
                                }
                            }.runTask(SGCraftAddon.instance);
                            break;
                        }

                        case "SMALL_EXPLODE": {
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    Fireball fireball = (Fireball) damager.getWorld().spawnEntity(damager.getLocation(), EntityType.FIREBALL);
                                    fireball.setVelocity(new Vector(0, -2, 0));
                                }
                            }.runTask(SGCraftAddon.instance);
                            break;
                        }

                        case "LARGE_EXPLODE": {
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    TNTPrimed tnt = (TNTPrimed) damager.getWorld().spawnEntity(damager.getLocation(), EntityType.PRIMED_TNT);
                                    tnt.setFuseTicks(0);
                                }
                            }.runTask(SGCraftAddon.instance);
                            break;
                        }
                    }
                }
            }
        }

    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "RodOfSouls";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.PLAYER_INTERACT);
        triggers.add(Trigger.SNOWBALL_LAUNCH);
        triggers.add(Trigger.SNOWBALL_HIT);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("sgcraftaddon.citem.rodofsouls");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&5Rod Of Souls");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7A legendary fishing rod crafted by Zenya"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Left click to change mode"));
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to use"));
        desc.add("");
        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.FISHING_ROD;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8Mode", "KNOCKBACK");
        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}
