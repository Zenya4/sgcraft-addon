package com.zenya.sgcraftaddon.items;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.Trigger;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.LoreUtils;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class HomingBow implements CItem {
    private LoreUtils loreUtils = LoreUtils.getInstance();
    private Entity target = null;

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case ENTITY_SHOOT_BOW: {
                EntityShootBowEvent e = (EntityShootBowEvent) event;
                Player player = (Player) e.getEntity();
                Arrow oldArrow = (Arrow) e.getProjectile();

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        e.setCancelled(true);

                        final int fireTicks = oldArrow.getFireTicks();
                        final int knockbackStrength = oldArrow.getKnockbackStrength();
                        final boolean critical = oldArrow.isCritical();

                        ItemStack bow = player.getInventory().getItemInHand();

                        if (player.getGameMode().equals(GameMode.CREATIVE) || player.getInventory().contains(Material.ARROW, 1)) {
                            if (!player.getGameMode().equals(GameMode.CREATIVE)) {
                                if(bow.getDurability() < 380) {

                                    bow.setDurability((short) (bow.getDurability() + 1));
                                    bow = player.getItemInHand();

                                } else {

                                    ItemStack brokenItem = new ItemStack(Material.AIR);
                                    player.setItemInHand(brokenItem);
                                    player.getWorld().playEffect(player.getLocation(), Effect.ZOMBIE_DESTROY_DOOR, 10);
                                    player.getWorld().playSound(player.getLocation(), "ENTITY_ITEM_BREAK", 0.4f, 0f);
                                }

                                ItemStack arrows = new ItemStack(Material.ARROW, 1);

                                if(!(bow.containsEnchantment(Enchantment.ARROW_INFINITE))) {
                                    player.getInventory().removeItem(arrows);
                                }
                            }

                            Arrow arrow = player.launchProjectile(Arrow.class);
                            arrow.setBounce(false);
                            arrow.setVelocity(oldArrow.getVelocity());
                            arrow.setShooter(player);
                            arrow.setFireTicks(fireTicks); // Set the new arrows on fire if the original one was
                            arrow.setKnockbackStrength(knockbackStrength);
                            arrow.setCritical(critical);
                            player.getWorld().playEffect(player.getLocation(),Effect.BOW_FIRE, 20);

                            double maxDist = 100;
                            for(Player pl : Bukkit.getOnlinePlayers()) {
                                if(pl.equals(e.getEntity()) || pl.getGameMode().equals(GameMode.SPECTATOR) || pl.hasPotionEffect(PotionEffectType.INVISIBILITY)) continue;
                                if(pl.getWorld().equals(arrow.getWorld())) {
                                    if(pl.getLocation().distance(arrow.getLocation()) < maxDist) {
                                        target = pl;
                                        maxDist = pl.getLocation().distance(arrow.getLocation());
                                    }
                                }
                            }

                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    if(target == null || arrow == null) this.cancel();
                                    Vector eyeLoc = new Vector(target.getLocation().getX(), target.getLocation().getY()+2, target.getLocation().getZ());
                                    arrow.setVelocity((eyeLoc.subtract(arrow.getLocation().toVector())).normalize().multiply(3));
                                }
                            }.runTaskTimer(SGCraftAddon.instance, 2L, 1L);
                        }
                        return;
                    }
                }
                ChatUtils.sendMessage(player, "&cYou do not have permission to use this item");
                e.setCancelled(true);
                break;
            }
        }
    }

    @Override
    public void onPassive(Player player) {

    }

    @Override
    public String getKey() {
        return "HomingBow";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.ENTITY_SHOOT_BOW);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("sgcraftaddon.admin");
        permissions.add("citem.use.homingbow");
        return permissions;
    }

    @Override
    public String getName() {
        return ChatColor.translateAlternateColorCodes('&', "&bHoming Bow");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7Essentially a bow which gives you aimbot"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to use"));
        desc.add("");

        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.BOW;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}

