package com.zenya.sgcraftaddon.scheduler;

import com.zenya.sgcraftaddon.interfaces.EventTask;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.ValueStorage;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Random;

public class SnowScheduler implements EventTask {
    ValueStorage valueStorage = ValueStorage.getInstance();
    ArrayList<Player> players;
    Random randObj = new Random();

    Integer density = 5;
    Integer range = 32;

    public SnowScheduler() {
        updateData();
        runTasks();
    }

    @Override
    public void runTasks() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(!(valueStorage.letItSnow)) {
                    return;
                }

                if(players != null && players.size() != 0) {
                    for (Player player : players) {
                        World world = player.getWorld();

                        // Sync add snow fallingblock
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                for (int i = 0; i < density; i++) {
                                    Location loc = player.getLocation();
                                    loc.setX(loc.getX() + randObj.nextInt(range * 2) - range);
                                    loc.setY((double) 128);
                                    loc.setZ(loc.getZ() + randObj.nextInt(range * 2) - range);
                                    @SuppressWarnings("deprecation") FallingBlock snow = world.spawnFallingBlock(loc, Material.SNOW, (byte) 0);
                                }
                            }
                        }.runTask(SGCraftAddon.instance);
                    }
                }
            }
        }.runTaskTimer(SGCraftAddon.instance, 0, 5);
    }

    @Override
    public void updateData() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(!(valueStorage.letItSnow)) {
                    return;
                }

                players = new ArrayList<Player>();

                for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                    if(player.getWorld().getEnvironment().equals(World.Environment.THE_END)) {
                        players.add(player);
                    }
                }

                if(players == null || players.size() == 0) {
                    return;
                }
            }
        }.runTaskTimerAsynchronously(SGCraftAddon.instance, 0, 5);
    }
}
