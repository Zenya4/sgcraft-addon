package com.zenya.sgcraftaddon.scheduler;

import com.zenya.sgcraftaddon.interfaces.CItemTask;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class CItemScheduler {
    private static CItemScheduler instance;
    private ArrayList<CItemTask> tasks = new ArrayList<CItemTask>();

    private CItemScheduler() {
        new BukkitRunnable() {
            @Override
            public void run() {

                for(CItemTask task : new ArrayList<CItemTask>(tasks)) {
                    if(task.getHealth() <= 0) {
                        tasks.remove(task);
                        continue;
                    }
                    task.reduceHealth();
                    task.runTask();
                }
            }
        }.runTaskTimerAsynchronously(SGCraftAddon.instance, 1L, 1L);
    }

    public void addTask(CItemTask task) {
        tasks.add(task);
    }

    public static CItemScheduler getInstance() {
        if(instance == null) {
            instance = new CItemScheduler();
        }
        return instance;
    }
}
