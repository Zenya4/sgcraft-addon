package com.zenya.sgcraftaddon.scheduler;

import com.zenya.sgcraftaddon.interfaces.EventTask;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.utilities.Circle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class TornadoScheduler implements EventTask {

    private final Integer cps = 1000;
    private final Integer tickDelay = 2;

    private static Integer timeLeft;

    private Location location;
    private Integer radius;
    private ArrayList<Double[]> outline;

    private ArrayList<Block> blocks = new ArrayList<Block>();
    private HashMap<FallingBlock, Integer> flyingBlocks = new HashMap<>();

    public TornadoScheduler(Location location, Integer radius, Integer time) {
        this.location = location;
        this.radius = radius;
        this.timeLeft = time;

        Circle circle = new Circle(new Integer[]{(int) location.getX(), (int) location.getZ()}, radius, 6.0);
        this.outline = circle.generateOutline();

        updateData();
        runTasks();
    }

    @Override
    public void runTasks() {
        //Random randObj = new Random();

        new BukkitRunnable() {
            @Override
            public void run() {
                if(timeLeft <= 0) {
                    return;
                }

                if(blocks != null && blocks.size() != 0) {
                    Block b = blocks.get(0);
                    Location fbLoc = new Location(b.getWorld(), b.getX(), b.getY()+2, b.getZ());

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            FallingBlock fb = b.getWorld().spawnFallingBlock(fbLoc, b.getType(), (byte) 0);
                            b.getWorld().getBlockAt(b.getLocation()).setType(Material.AIR);

                            flyingBlocks.put(fb, 0);
                        }
                    }.runTask(SGCraftAddon.instance);

                    blocks.remove(0);
                }

                for(Map.Entry<FallingBlock, Integer> entry : flyingBlocks.entrySet()) {
                    FallingBlock fb  = entry.getKey();
                    Integer outlineIndex = entry.getValue();

                    //Float randomiser = randObj.nextFloat();

                    Vector currentLoc = new Vector(fb.getLocation().getX(), 0, fb.getLocation().getZ());
                    Vector newLoc = new Vector(outline.get(outlineIndex)[0], 0, outline.get(outlineIndex)[1]);
                    Vector vel = newLoc.subtract(currentLoc).normalize().setY(0.2);

                    fb.setVelocity(vel);

                    if(outlineIndex < (outline.size()-1)) {
                        outlineIndex++;
                    } else {
                        outlineIndex = 0;
                    }
                    entry.setValue(outlineIndex);
                }
            }
        }.runTaskTimerAsynchronously(SGCraftAddon.instance, 0L, tickDelay);
    }

    @Override
    public void updateData() {
        World world = location.getWorld();
        Integer locX = location.getBlockX();
        Integer locZ = location.getBlockZ();
        Random randObj = new Random();

        new BukkitRunnable() {
            @Override
            public void run() {
                if(timeLeft <= 0) {
                    return;
                }

                for(int i=0; i<cps; i++) {
                    Integer blockX = locX + randObj.nextInt(radius + 1) - randObj.nextInt(radius + 1);
                    Integer blockZ = locZ + randObj.nextInt(radius + 1) - randObj.nextInt(radius + 1);
                    Integer blockY = randObj.nextInt(257);
                    Block block = world.getBlockAt(new Location(world, blockX, blockY, blockZ));
                    Block blockUp = world.getBlockAt(new Location(world, blockX, blockY+1, blockZ));

                    if(!(block == null) && !(block.getType().equals(Material.AIR)) && !(block.getType().equals(Material.CAVE_AIR)) && !(blockUp == null) && blockUp.getType().equals(Material.AIR)) {
                        blocks.add(block);
                    }
                }
                timeLeft--;
            }
        }.runTaskTimerAsynchronously(SGCraftAddon.instance, 0L, 20L);
    }
}
