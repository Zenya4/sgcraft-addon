package com.zenya.sgcraftaddon.scheduler;

import com.zenya.sgcraftaddon.interfaces.EventTask;
import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.storage.ValueStorage;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Random;

public class DragonScheduler implements EventTask {
    ValueStorage valueStorage = ValueStorage.getInstance();
    ArrayList<World> endWorlds;
    ArrayList<EnderDragon> dragons;
    Random randObj = new Random();

    Integer streams = 5;

    public DragonScheduler() {
        updateData();
        runTasks();
    }

    public void runTasks() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(!(valueStorage.betterDragon)) {
                    return;
                }
                if(dragons != null || dragons.size() != 0) {
                    for(EnderDragon dragon : dragons) {
                        Location loc = dragon.getLocation();
                        World world = loc.getWorld();

                        switch(dragon.getPhase()) {
                            case BREATH_ATTACK: {

                            }

                            case CHARGE_PLAYER: {

                            }

                            case CIRCLING: {

                            }

                            case DYING: {

                            }

                            case FLY_TO_PORTAL: {

                            }

                            case LAND_ON_PORTAL: {

                            }

                            case LEAVE_PORTAL: {

                            }

                            case ROAR_BEFORE_ATTACK: {

                            }

                            case SEARCH_FOR_BREATH_ATTACK_TARGET: {

                            }

                            case STRAFING: {

                            }

                            default: {
                                // Sync add fire fallingblock
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        for(int i=0; i<streams; i++) {
                                            @SuppressWarnings("deprecation") FallingBlock fire = world.spawnFallingBlock(loc, Material.FIRE, (byte) 0);
                                            double x = dragon.getVelocity().getX() * (randObj.nextInt(2) + 2);
                                            double y = dragon.getVelocity().getY();
                                            double z = dragon.getVelocity().getZ() * (randObj.nextInt(2) + 2);
                                            fire.setVelocity(new Vector(x, y, z));
                                        }
                                    }
                                }.runTask(SGCraftAddon.instance);
                                break;
                            }
                        }
                    }
                }
            }
        }.runTaskTimer(SGCraftAddon.instance, 20, 1);
    }

    public void updateData() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(!(valueStorage.betterDragon)) {
                    return;
                }

                endWorlds = new ArrayList<World>();
                dragons = new ArrayList<EnderDragon>();

                for (World world : Bukkit.getServer().getWorlds()) {
                    if(world.getEnvironment().equals(World.Environment.THE_END)) {
                        endWorlds.add(world);
                    }
                }

                if(endWorlds == null || endWorlds.size() == 0) {
                    return;
                }

                for(World world : endWorlds) {
                    for(Entity entity : world.getEntities()) {
                        if(entity.getType().equals(EntityType.ENDER_DRAGON)) {
                            dragons.add((EnderDragon) entity);
                        }
                    }
                }
            }
        }.runTaskTimerAsynchronously(SGCraftAddon.instance, 0, 20);
    }
}
