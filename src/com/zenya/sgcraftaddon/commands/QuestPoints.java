package com.zenya.sgcraftaddon.commands;

import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.utilities.SQLiteManager;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class QuestPoints implements CommandExecutor {
SQLiteManager sqLiteManager = SGCraftAddon.sqLiteManager;

@Override
public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if(!(sender.hasPermission("questpoints.help")) && !(sender.hasPermission("sgcraftaddon.player"))) {
        ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
        return true;
    }

    if(args.length == 0) {
        // Send help
        ChatUtils.sendMessage(sender, "&6--- QuestPoints ---");
        ChatUtils.sendMessage(sender, "&e/questpoints get <player> ");
        ChatUtils.sendMessage(sender, "&e/questpoints top");
        ChatUtils.sendMessage(sender, "&e/questpoints set [player] [amount]");
        ChatUtils.sendMessage(sender, "&e/questpoints add [player] [amount] ");
        ChatUtils.sendMessage(sender, "&e/questpoints addall [amount] ");
        ChatUtils.sendMessage(sender, "&e/questpoints remove [player] [amount]");
        ChatUtils.sendMessage(sender, "&6--- QuestPoints ---");
        ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
        return true;
    }

    switch(args[0].toUpperCase()) {
        case "GET": {
            Player player;

            if(args.length == 1) { //get

                if(!(sender.hasPermission("questpoints.get")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }

                if(sender instanceof ConsoleCommandSender) {
                    ChatUtils.sendMessage(sender, "&c/questpoints get [player]");
                    return true;
                } else {
                    player = (Player) sender;
                }

            } else { //get <player>
                if(!(sender.hasPermission("questpoints.get.other")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }

                player = Bukkit.getServer().getPlayer(args[1]);

                if(player == null) {
                    ChatUtils.sendMessage(sender,"&4That specified player is not online or does not exist");
                    return true;
                }
            }

            // Get from DB
            try {
                Integer questpoints = sqLiteManager.getQuestPoints(player);
                ChatUtils.sendMessage(sender, "&6" + player.getName() + " has " + questpoints.toString() + " questpoints");
            } catch(Exception e) {
                ChatUtils.sendMessage(sender, "&4That specified player does not exist in the database. Try getting them to relog");
                return true;
            }
            break;
        }

        case "TOP": {
            if(!(sender.hasPermission("questpoints.top")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                return true;
            }

            Map<String, Integer> leaderboard = new HashMap<String, Integer>();
            leaderboard = sqLiteManager.getTopQuestPoints(5);

            for(Map.Entry<String, Integer> entry : leaderboard.entrySet()) {
                ChatUtils.sendMessage(sender, "&6" + entry.getKey() + ": " + entry.getValue() + " questpoints");
            }
            break;
        }

        case "SET": {
            Player player;
            Integer questpoints;

            if(!(sender.hasPermission("questpoints.set")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
                ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                return true;
            }

            if(!(args.length == 3)) {
                ChatUtils.sendMessage(sender, "&c/questpoints set [player] [amount]");
                return true;
            }

            player = Bukkit.getServer().getPlayer(args[1]);

            if(player == null) {
                ChatUtils.sendMessage(sender,"&4That specified player is not online or does not exist");
                return true;
            }

            try {
                questpoints = Integer.parseInt(args[2]);
                if(questpoints < 0) {
                    throw new Exception("Amount must be a positive integer");
                }
            } catch(Exception e) {
                ChatUtils.sendMessage(sender, "&4Amount must be a positive integer");
                return true;
            }

            // Actually set
            sqLiteManager.setQuestPoints(player, questpoints);
            ChatUtils.sendMessage(sender, "&6You have set " + player.getName() + "\'s questpoint balance to: " + questpoints.toString() + " questpoints");

            if(!(sender.getName().equals(player.getName()))) {
                ChatUtils.sendMessage(player, "&6" + sender.getName() + " has set your questpoints balance to: " + questpoints.toString() + " questpoints");
            }
            break;
        }


        case "ADD": {
            Player player;
            Integer questpoints;
            Integer questpointsold;
            Integer questpointsnew;

            if(!(sender.hasPermission("questpoints.add")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
                ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                return true;
            }

            if(!(args.length == 3)) {
                ChatUtils.sendMessage(sender, "&c/questpoints add [player] [amount]");
                return true;
            }

            player = Bukkit.getServer().getPlayer(args[1]);

            if(player == null) {
                ChatUtils.sendMessage(sender,"&4That specified player is not online or does not exist");
                return true;
            }

            try {
                questpoints = Integer.parseInt(args[2]);
                if(questpoints <= 0) {
                    throw new Exception("Amount must be a positive integer");
                }
            } catch(Exception e) {
                ChatUtils.sendMessage(sender, "&4Amount must be a positive integer");
                return true;
            }

            // Get from DB
            try {
                questpointsold = sqLiteManager.getQuestPoints(player);
            } catch(Exception e) {
                ChatUtils.sendMessage(sender, "&4That specified player does not exist in the database. Try getting them to relog");
                return true;
            }

            // Actually set
            questpointsnew = questpointsold + questpoints;
            sqLiteManager.setQuestPoints(player, questpointsnew);
            ChatUtils.sendMessage(sender, "&6You have added " + questpoints.toString() + " questpoints to " + player.getName() + ". They now have " + questpointsnew.toString() + " questpoints");

            if(!(sender.getName().equals(player.getName()))) {
                ChatUtils.sendMessage(player, "&6" + sender.getName() + " has added " + questpoints.toString() + " questpoints to your balance. You now have " + questpointsnew.toString() + " questpoints");
            }
            break;
        }

        case "ADDALL": {
            Integer questpoints;
            Integer questpointsold;
            Integer questpointsnew;

            if(!(sender.hasPermission("questpoints.addall")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
                ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                return true;
            }

            if(!(args.length == 2)) {
                ChatUtils.sendMessage(sender, "&c/questpoints addall [amount]");
                return true;
            }

            try {
                questpoints = Integer.parseInt(args[1]);
                if(questpoints <= 0) {
                    throw new Exception("Amount must be a positive integer");
                }
            } catch(Exception e) {
                ChatUtils.sendMessage(sender, "&4Amount must be a positive integer");
                return true;
            }

            // Get from DB
            for(Player player : Bukkit.getServer().getOnlinePlayers()) {
                try {
                    questpointsold = sqLiteManager.getQuestPoints(player);
                } catch(Exception e) {
                    continue;
                }

                // Actually set
                questpointsnew = questpointsold + questpoints;
                sqLiteManager.setQuestPoints(player, questpointsnew);

                if(!(sender.getName().equals(player.getName()))) {
                    ChatUtils.sendMessage(player, "&6" + sender.getName() + " has added " + questpoints.toString() + " questpoints to your balance. You now have " + questpointsnew.toString() + " questpoints");
                }
            }
            ChatUtils.sendMessage(sender, "&6You have added " + questpoints.toString() + " questpoints to everybody");
            break;
        }

        case "REMOVE": {
            Player player;
            Integer questpoints;
            Integer questpointsold;
            Integer questpointsnew;

            if(!(sender.hasPermission("questpoints.remove")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
                ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                return true;
            }

            if (!(args.length == 3)) {
                ChatUtils.sendMessage(sender, "&c/questpoints remove [player] [amount]");
                return true;
            }

            player = Bukkit.getServer().getPlayer(args[1]);

            if (player == null) {
                ChatUtils.sendMessage(sender,"&4That specified player is not online or does not exist");
                return true;
            }

            try {
                questpoints = Integer.parseInt(args[2]);
                if (questpoints <= 0) {
                    throw new Exception("Amount must be a positive integer");
                }
            } catch (Exception e) {
                ChatUtils.sendMessage(sender, "&4Amount must be a positive integer");
                return true;
            }

            // Get from DB
            try {
                questpointsold = sqLiteManager.getQuestPoints(player);
            } catch (Exception e) {
                ChatUtils.sendMessage(sender, "&4That specified player does not exist in the database. Try getting them to relog");
                return true;
            }

            // Actually set
            questpointsnew = questpointsold - questpoints;
            if (questpointsnew < 0) {
                ChatUtils.sendMessage(sender, "&4" + player.getName() + " has insufficent questpoints (" + questpointsold.toString() + ")");
                return true;
            }

            sqLiteManager.setQuestPoints(player, questpointsnew);
            ChatUtils.sendMessage(sender, "&6You have removed " + questpoints.toString() + " questpoints from " + player.getName() + ". They now have " + questpointsnew.toString() + " questpoints");

            if(!(sender.getName().equals(player.getName()))) {
                ChatUtils.sendMessage(player, "&6" + sender.getName() + " has removed " + questpoints.toString() + " questpoints from your balance. You now have " + questpointsnew.toString() + " questpoints");
            }
            break;
        }

            default: {
                if(!(sender.hasPermission("questpoints.help")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }
                // Send help
                ChatUtils.sendMessage(sender, "&6--- QuestPoints ---");
                ChatUtils.sendMessage(sender, "&e/questpoints get <player> ");
                ChatUtils.sendMessage(sender, "&e/questpoints top");
                ChatUtils.sendMessage(sender, "&e/questpoints set [player] [amount]");
                ChatUtils.sendMessage(sender, "&e/questpoints add [player] [amount] ");
                ChatUtils.sendMessage(sender, "&e/questpoints addall [amount] ");
                ChatUtils.sendMessage(sender, "&e/questpoints remove [player] [amount]");
                ChatUtils.sendMessage(sender, "&6--- QuestPoints ---");
                ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
                break;
            }
        }
    return true;
    }
}
