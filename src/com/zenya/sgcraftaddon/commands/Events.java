package com.zenya.sgcraftaddon.commands;

import com.zenya.sgcraftaddon.scheduler.DragonScheduler;
import com.zenya.sgcraftaddon.scheduler.SnowScheduler;
import com.zenya.sgcraftaddon.scheduler.TornadoScheduler;
import com.zenya.sgcraftaddon.storage.ValueStorage;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Events implements CommandExecutor {
    ValueStorage valueStorage = ValueStorage.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender.hasPermission("events.help")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
            ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
            return true;
        }

        if(args.length == 0) {
            // Send help
            ChatUtils.sendMessage(sender, "&6--- Events ---");
            ChatUtils.sendMessage(sender, "&e/events betterdragon");
            ChatUtils.sendMessage(sender, "&e/events letitsnow");
            ChatUtils.sendMessage(sender, "&e/events snowballboom");
            ChatUtils.sendMessage(sender, "&e/events tornado");
            ChatUtils.sendMessage(sender, "&6--- Events ---");
            ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
            return true;
        }

        switch(args[0].toUpperCase()) {
            case "BETTERDRAGON": {
                if(!(sender.hasPermission("events.betterdragon")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }

                if(valueStorage.betterDragon) {
                    valueStorage.betterDragon = false;
                    ChatUtils.sendMessage(sender, "&cDragons are now back to normal");
                } else {
                    valueStorage.betterDragon = true;
                    new DragonScheduler();
                    ChatUtils.sendMessage(sender, "&aDragons are now way cooler");
                }
                break;
            }

            case "LETITSNOW": {
                if(!(sender.hasPermission("events.letitsnow")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }

                if(valueStorage.letItSnow) {
                    valueStorage.letItSnow = false;
                    ChatUtils.sendMessage(sender, "&cSnow is now back to normal");
                } else {
                    valueStorage.letItSnow = true;
                    new SnowScheduler();
                    ChatUtils.sendMessage(sender, "&aMerry christmas!");
                }
                break;
            }

            case "SNOWBALLBOOM": {
                if(!(sender.hasPermission("events.snowballboom")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }

                if(valueStorage.snowballBoom) {
                    valueStorage.snowballBoom = false;
                    ChatUtils.sendMessage(sender, "&cSnowballs are now back to normal");
                } else {
                    valueStorage.snowballBoom = true;
                    ChatUtils.sendMessage(sender, "&aWho put gunpowder in my snowballs?");
                }
                break;
            }

            case "TORNADO": {
                if(!(sender.hasPermission("events.tornado")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }

                ChatUtils.sendMessage(sender, "&cA storm is brewing...");
                new TornadoScheduler(((Player) sender).getLocation(), 15, 120);
                break;
            }

            default: {
                // Send help
                ChatUtils.sendMessage(sender, "&6--- Events ---");
                ChatUtils.sendMessage(sender, "&e/events betterdragon");
                ChatUtils.sendMessage(sender, "&e/events letitsnow");
                ChatUtils.sendMessage(sender, "&e/events snowballboom");
                ChatUtils.sendMessage(sender, "&e/events tornado");
                ChatUtils.sendMessage(sender, "&6--- Events ---");
                ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
                break;
            }
        }
        return true;
    }
}
