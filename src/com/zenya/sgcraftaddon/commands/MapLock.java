package com.zenya.sgcraftaddon.commands;

import com.zenya.sgcraftaddon.utilities.LoreUtils;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class MapLock implements CommandExecutor {
    LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender.hasPermission("maplock.help")) && !(sender.hasPermission("sgcraftaddon.player"))) {
            ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
            return true;
        }

        if(args.length == 0) {
            // Send help
            ChatUtils.sendMessage(sender, "&6--- MapLock ---");
            ChatUtils.sendMessage(sender, "&e/maplock lock");
            ChatUtils.sendMessage(sender, "&e/maplock unlock");
            ChatUtils.sendMessage(sender, "&6--- MapLock ---");
            ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
            return true;
        }

        Player player;

        if(sender instanceof Player) {
            player = (Player) sender;
        } else {
            ChatUtils.sendMessage(sender, "&4This command can only be used by a player");
            return true;
        }

        ItemStack item = player.getInventory().getItemInMainHand();
        ItemStack oitem = item;

        if(!(item.getType().equals(Material.FILLED_MAP))) {
            ChatUtils.sendMessage(player, "&cYou must be holding a filled map in your hand to do this");
            return true;
        }

        switch(args[0].toUpperCase()) {

            case "LOCK": {
                if(!(player.hasPermission("maplock.lock")) && !(player.hasPermission("sgcraftaddon.player"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }

                if(loreUtils.getKey(item, "&9Locked by") != null) {
                    ChatUtils.sendMessage(player, "&cThis map has already been locked");
                    return true;
                }

                // Begin locking
                item = loreUtils.setKey(item, "&9Locked by", player.getName());

                player.getInventory().remove(oitem);
                player.getInventory().addItem(item);
                ChatUtils.sendMessage(player, "&aThis map has successfully been locked");
                break;
            }
            case "UNLOCK": {
                if(!(player.hasPermission("maplock.unlock")) && !(player.hasPermission("sgcraftaddon.player"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }

                if(loreUtils.getKey(item, "&9Locked by") != null) {
                    ChatUtils.sendMessage(player, "&cThis map is not locked");
                    return true;
                }

                if(!(player.hasPermission("maplock.unlock.other")) && !(player.hasPermission("sgcraftaddon.admin"))) {
                    if(!(loreUtils.getKey(item, "&9Locked by").equals(player.getName()))) {
                        ChatUtils.sendMessage(player, "&cYou cannot unlock another player\'s map");
                        return true;
                    }
                }

                // Begin unlocking
                item = loreUtils.removeKey(item, "&9Locked by");

                player.getInventory().remove(oitem);
                player.getInventory().addItem(item);
                ChatUtils.sendMessage(player, "&aThis map has successfully been unlocked");
                break;
            }

            default: {
                // Send help
                ChatUtils.sendMessage(sender, "&6--- MapLock ---");
                ChatUtils.sendMessage(sender, "&e/questpoints lock");
                ChatUtils.sendMessage(sender, "&e/questpoints unlock");
                ChatUtils.sendMessage(sender, "&6--- MapLock ---");
                ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
                break;
            }
        }
        return true;
    }
}
