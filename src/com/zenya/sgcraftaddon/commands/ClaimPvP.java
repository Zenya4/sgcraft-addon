package com.zenya.sgcraftaddon.commands;

import com.zenya.sgcraftaddon.storage.ClaimPvPStorage;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.GriefPreventionHook;
import me.ryanhamshire.GriefPrevention.Claim;
import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ClaimPvP implements CommandExecutor {

    ClaimPvPStorage storage = ClaimPvPStorage.getInstance();
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String cmdlabel, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        if (!cmd.getName().equalsIgnoreCase("claimpvp")) {
            return true;
        }
        Player p = (Player) sender;
        Claim claim = GriefPreventionHook.getClaimAtLocation(p, p.getLocation());
        Boolean result = storage.isPvpEnabled(claim);
        if (claim == null) {
            ChatUtils.sendMessage(p, "&c&lOops! &7You're not in a claim!");
            return true;
        }
        if (result == null) {
            storage.setPVP(claim, false);
            result = false;
        }
        if (!p.hasPermission("claimpvp.change") && !p.hasPermission("sgcraftaddon.player")) { ChatUtils.sendMessage(p, "&cYou can't do that!"); return true;
        }
        if (!p.hasPermission("claimpvp.change.others") && !p.hasPermission("sgcraftaddon.admin")) {
            if (!(GriefPreventionHook.isOwnerAtLocation(p, p.getLocation())) && (!(GriefPreventionHook.isManagerAtClaim(claim, p)))) {
                if (result == false) {
                    ChatUtils.sendMessage(p, "&a" + claim.getOwnerName() + "&7: &ePVP &7is &2disabled &7here.");
                }
                else {
                    ChatUtils.sendMessage(p, "&c" + claim.getOwnerName() + "&7: &ePVP &7is &cenabled &7here.");
                }
                return true;
            }
        }
        if (args.length > 0) {
            switch (args[0]) {
                case "off":
                    storage.setPVP(claim, false);
                    ChatUtils.sendMessage(p, " ");
                    ChatUtils.sendMessage(p, "&2&l                        ! ");
                    ChatUtils.sendMessage(p, "&e             PVP &fhas been &cdisabled &fin this claim.");
                    ChatUtils.sendMessage(p, " ");
                    ArrayList<Chunk> c = claim.getChunks();
                    ArrayList<Player> inr = new ArrayList<Player>();
                    for (Chunk ch : c) {
                        Entity[] e = ch.getEntities();
                        for (Entity en : e) {
                            if (!(en instanceof Player)) {
                                return true;
                            }
                            inr.add((Player) en);
                        }
                    }
                    return true;
                case "on":
                    Long cooldown = storage.getRawCooldown(p.getUniqueId());
                    if (cooldown == null) cooldown = 0L;
                    Long ccooldown = (System.currentTimeMillis() - cooldown);
                    if (ccooldown < 60000) {
                        ChatUtils.sendMessage(p, " ");
                        ChatUtils.sendMessage(p, "&c&lOops! &7It's on cooldown!");
                        ChatUtils.sendMessage(p, " ");
                        return true;
                    }
                    storage.setPVP(claim, true);
                    ChatUtils.sendMessage(p, " ");
                    ChatUtils.sendMessage(p, "&4&l                        ! ");
                    ChatUtils.sendMessage(p, "&e             PVP &fhas been &aenabled &fin this claim.");
                    ChatUtils.sendMessage(p, " ");
                    storage.setCooldown(p.getUniqueId(), System.currentTimeMillis());
                    ArrayList<Chunk> cr = claim.getChunks();
                    ArrayList<Player> inrr = new ArrayList<Player>();
                    for (Chunk ch : cr) {
                        Entity[] e = ch.getEntities();
                        for (Entity en : e) {
                            if (!(en instanceof Player)) {
                                return true;
                            }
                            inrr.add((Player) en);
                        }
                    }
                    return true;
                case "status":
                case "here":
                case "check":
                    if (!p.hasPermission("claimpvp.check")) { ChatUtils.sendMessage(p, "&c&lOops! &7You can't check pvp options!"); return true;}
                    Boolean doesp = storage.isPvpEnabled(claim);
                    if (doesp) {
                        ChatUtils.sendMessage(p, "&e&lPVP &7is &fenabled &7in here!");
                        return true;
                    }
                    ChatUtils.sendMessage(p, "&e&lPVP &7is &fdisabled &7in here.");
                    return true;
                default:
                    ChatUtils.sendMessage(p, "&c&lOops! &7Use '&f/claimpvp &non/off&f'&7.");
                    return true;
            }
        }
        else {
            if (result == true) {
                storage.setPVP(claim, false);
                ChatUtils.sendMessage(p, " ");
                ChatUtils.sendMessage(p, "&2&l                        ! ");
                ChatUtils.sendMessage(p, "&e             PVP &fhas been &cdisabled &fin this claim.");
                ChatUtils.sendMessage(p, " ");
                ArrayList<Chunk> c = claim.getChunks();
                ArrayList<Player> inr = new ArrayList<Player>();
                for (Chunk ch : c) {
                    Entity[] e = ch.getEntities();
                    for (Entity en : e) {
                        if (!(en instanceof Player)) {
                            return true;
                        }
                        inr.add((Player) en);
                    }
                }
                return true;
            }
            else {
                Long cooldown = storage.getRawCooldown(p.getUniqueId());
                if (cooldown == null) cooldown = 0L;
                Long ccooldown = (System.currentTimeMillis() - cooldown);
                if (ccooldown < 60000) {
                    ChatUtils.sendMessage(p, " ");
                    ChatUtils.sendMessage(p, "&c&lOops! &7It's on cooldown!");
                    ChatUtils.sendMessage(p, " ");
                    return true;
                }
                storage.setPVP(claim, true);
                ChatUtils.sendMessage(p, " ");
                ChatUtils.sendMessage(p, "&4&l                        ! ");
                ChatUtils.sendMessage(p, "&e             PVP &fhas been &aenabled &fin this claim.");
                ChatUtils.sendMessage(p, " ");
                storage.setCooldown(p.getUniqueId(), System.currentTimeMillis());
                ArrayList<Chunk> c = claim.getChunks();
                ArrayList<Player> inr = new ArrayList<Player>();
                for (Chunk ch : c) {
                    Entity[] e = ch.getEntities();
                    for (Entity en : e) {
                        if (!(en instanceof Player)) {
                            return true;
                        }
                        inr.add((Player) en);
                    }
                }
                return true;
            }
        }
    }

}
