package com.zenya.sgcraftaddon.commands;

import com.zenya.sgcraftaddon.main.SGCraftAddon;
import com.zenya.sgcraftaddon.utilities.PointsToRanks;
import com.zenya.sgcraftaddon.utilities.SQLiteManager;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import com.zenya.sgcraftaddon.utilities.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class QuestRanks implements CommandExecutor {
    ConfigManager configManager = SGCraftAddon.configManager;
    SQLiteManager sqLiteManager = SGCraftAddon.sqLiteManager;
    PointsToRanks pointsToRanks = new PointsToRanks();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender.hasPermission("questranks.help")) && !(sender.hasPermission("sgcraftaddon.player"))) {
            ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
            return true;
        }

        if(args.length == 0) {
            // Send help
            ChatUtils.sendMessage(sender, "&6--- QuestRanks ---");
            ChatUtils.sendMessage(sender, "&e/questranks get <player> ");
            ChatUtils.sendMessage(sender, "&e/questranks list");
            ChatUtils.sendMessage(sender, "&6--- QuestRanks ---");
            ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
            return true;
        }

        switch(args[0].toUpperCase()) {
            case "GET": {
                Player player;
                Integer questpoints;
                String rank;
                String display;

                if(args.length == 1) { //get

                    if(!(sender.hasPermission("questranks.get")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                        ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                        return true;
                    }

                    if(sender instanceof ConsoleCommandSender) {
                        ChatUtils.sendMessage(sender, "&c/questranks get [player]");
                        return true;
                    } else {
                        player = (Player) sender;
                    }

                } else { //get <player>
                    if(!(sender.hasPermission("questranks.get.other")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                        ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                        return true;
                    }

                    player = Bukkit.getServer().getPlayer(args[1]);

                    if(player == null) {
                        ChatUtils.sendMessage(sender, "&4That specified player is not online or does not exist");
                        return true;
                    }
                }

                // Get from DB
                try {
                    questpoints = sqLiteManager.getQuestPoints(player);
                } catch(Exception e) {
                    ChatUtils.sendMessage(sender, "&4That specified player does not exist in the database. Try getting them to relog");
                    return true;
                }

                // Convert
                rank = pointsToRanks.convert(questpoints);
                display = configManager.getRankDisplay(rank);

                ChatUtils.sendMessage(sender, "&6" + player.getName() + " has the questrank: " + display);
                break;
            }

            case "LIST": {
                String rank;
                String display;
                Integer qpCost;

                if(!(sender.hasPermission("questranks.list")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }

                ArrayList<String> ranks = new ArrayList<String>();
                ranks = configManager.getRanks();

                ChatUtils.sendMessage(sender, "&b--- QuestRanks ---");
                for(int i=ranks.size()-1; i>-1; i--) {
                    rank = ranks.get(i);
                    display = configManager.getRankDisplay(rank);
                    qpCost = configManager.getRankQP(rank);
                    ChatUtils.sendMessage(sender,display + "&f" + " - " + qpCost.toString() + " questpoints");
                }
                ChatUtils.sendMessage(sender, "&b--- QuestRanks ---");
                break;
            }

            default: {
                if(!(sender.hasPermission("questranks.help")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                    ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
                    return true;
                }
                // Send help
                ChatUtils.sendMessage(sender, "&6--- QuestRanks ---");
                ChatUtils.sendMessage(sender, "&e/questranks get <player> ");
                ChatUtils.sendMessage(sender, "&e/questranks list");
                ChatUtils.sendMessage(sender, "&6--- QuestRanks ---");
                ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
                break;
            }
        }
        return true;
    }
}
