package com.zenya.sgcraftaddon.commands;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.inventory.ToolBoxGUI;
import com.zenya.sgcraftaddon.storage.CItemStorage;
import com.zenya.sgcraftaddon.utilities.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class ToolBox implements CommandExecutor {
    private ToolBoxGUI toolBoxGUI = ToolBoxGUI.getInstance();
    private static CItemStorage cItemStorage = CItemStorage.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender.hasPermission("toolbox.use")) && !(sender.hasPermission("sgcraftaddon.admin"))) {
            ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
            return true;
        }

        switch(args.length) {
            case 0: {
                if(sender instanceof ConsoleCommandSender) {
                    ChatUtils.sendMessage(sender, "&6--- ToolBox ---");
                    ChatUtils.sendMessage(sender, "&e/toolbox");
                    ChatUtils.sendMessage(sender, "&e/toolbox give [player] [item]");
                    ChatUtils.sendMessage(sender, "&6--- ToolBox ---");
                    ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
                    return true;
                } else {
                    toolBoxGUI.openInventory((Player) sender);
                }
                break;
            }
            case 3: {
                Player player = Bukkit.getPlayer(args[1]);
                CItem cItem = cItemStorage.getCItem(args[2]);

                if(!args[0].equals("give")) {
                    return false;
                }

                if(player == null) {
                    ChatUtils.sendMessage(sender, "&4That player is not online");
                    return true;
                }

                if(cItem == null) {
                    ChatUtils.sendMessage(sender, "&4That item does not exist. Note that items are case-sensitive");
                    return true;
                }

                player.getInventory().addItem(cItem.getItem());
                ChatUtils.sendMessage(sender, "&e" + player.getName() + " &6has been given a " + cItem.getName());
                break;
            }
            default: {
                ChatUtils.sendMessage(sender, "&6--- ToolBox ---");
                ChatUtils.sendMessage(sender, "&e/toolbox");
                ChatUtils.sendMessage(sender, "&e/toolbox give [player] [item]");
                ChatUtils.sendMessage(sender, "&6--- ToolBox ---");
                ChatUtils.sendMessage(sender, "&cMade with &4<3 &cfor SGCraft by Zenya");
                break;
            }
        }
        return true;
    }
}
