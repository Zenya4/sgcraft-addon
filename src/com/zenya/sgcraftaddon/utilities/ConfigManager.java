package com.zenya.sgcraftaddon.utilities;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class ConfigManager {

    Plugin plugin;
    FileConfiguration config;

    public ConfigManager(Plugin plugin) {
        this.plugin = plugin;
        this.config = plugin.getConfig();

        if(!(this.getConfigExists())) {
            plugin.saveDefaultConfig();
        }
    }

    public boolean getConfigExists() {
        File configFile = new File(plugin.getDataFolder(), "config.yml");
        return configFile.exists();
    }

    public String getConfigVersion() {
        String version = config.getString("config-version");
        return version;
    }

    public ArrayList<String> getRanks() {
        ArrayList<String> ranks = new ArrayList<String>();

        for(String key : config.getConfigurationSection("ranks").getKeys(false)) {
            ranks.add(key);
        }
        return ranks;
    }

    public String getRankDisplay(String rank) {
        String display;

        try {
            display = config.getString("ranks." + rank + ".display");
        } catch(Exception e) {
            display = "";
        }
        return ChatColor.translateAlternateColorCodes('&', display);
    }

    public int getRankQP(String rank) {

        Integer qpCost;

        try {
        qpCost = config.getInt("ranks." + rank + ".questpoints");
        } catch(Exception e) {
        qpCost = -1;
        }
        return qpCost;
    }
}

