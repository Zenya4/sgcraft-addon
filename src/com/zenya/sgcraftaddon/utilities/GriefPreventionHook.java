package com.zenya.sgcraftaddon.utilities;

import org.bukkit.Location;

import org.bukkit.entity.Player;

import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import me.ryanhamshire.GriefPrevention.PlayerData;

public class GriefPreventionHook {

    public static GriefPreventionHook instance;

    public GriefPreventionHook()
    {
        instance = this;
    }
    public static GriefPreventionHook getHook() {
        if (instance == null) {
            instance = new GriefPreventionHook();
        }
        return instance;
    }
    public static Claim getClaimAtLocation(Player p, Location loc) {
        PlayerData playerData = GriefPrevention.instance.dataStore.getPlayerData(p.getUniqueId());
        return GriefPrevention.instance.dataStore.getClaimAt(loc, false, playerData.lastClaim);
    }

    public static boolean hasClaimInLocation(Player p, Location loc) {
        return getClaimAtLocation(p, loc) != null;
    }

    public static boolean isOwnerAtLocation(Player p, Location loc) {
        if (!hasClaimInLocation(p, loc))
            return false;
        return getClaimAtLocation(p, loc).getOwnerName().equalsIgnoreCase(p.getName());
    }
    public static boolean isManagerAtClaim(Claim claim, Player p) {
        if (claim.managers.contains(p)) {
            return true;
        }
        return false;
    }
}
