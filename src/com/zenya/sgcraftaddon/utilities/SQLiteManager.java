package com.zenya.sgcraftaddon.utilities;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class SQLiteManager {

    Plugin plugin;;

    public SQLiteManager(Plugin plugin) {
        this.plugin = plugin;

        if(!(getDatabaseExists())) {
            plugin.getDataFolder().mkdir();
        }

        this.createTables();
    }

    public boolean getDatabaseExists() {
        File databaseFile = new File(plugin.getDataFolder(), "database.db");
        return databaseFile.exists();
    }

    public Connection connect() {
        String url = "jdbc:sqlite:" + plugin.getDataFolder() + File.separator + "database.db";
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public void createTables() {
        String sql = "CREATE TABLE IF NOT EXISTS questpoints ("
                + "id integer PRIMARY KEY AUTOINCREMENT, "
                + "player string NOT NULL UNIQUE, "
                + "questpoints integer NOT NULL);";

        try {
            Connection conn = this.connect();
            Statement statement = conn.createStatement();
            statement.execute(sql);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Integer getQuestPoints(Player player) {
        Integer questpoints = 0;
        String playername = player.getName();

        String sql = "SELECT questpoints FROM questpoints WHERE player = ?";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, playername);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                questpoints = rs.getInt("questpoints");
            }

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return questpoints;
    }

    public Map<String, Integer> getTopQuestPoints(Integer amount) {
        Map<String, Integer> leaderboard = new HashMap<String, Integer>();
        Integer count = 0;
        String sql = "SELECT player, questpoints FROM questpoints ORDER BY questpoints DESC";

        try {
            Connection conn = this.connect();
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            while(rs.next() && count < amount) {
                leaderboard.put(rs.getString("player"), rs.getInt("questpoints"));
                count++;
            }

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return leaderboard;
    }

    public void initQuestPoints(Player player, Integer questpoints) {
        String playername = player.getName();

        String sql = "INSERT OR IGNORE INTO questpoints(player, questpoints) VALUES(?, ?)";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, playername);
            ps.setInt(2, questpoints);
            ps.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setQuestPoints(Player player, Integer questpoints) {
        String playername = player.getName();

        String sql = "UPDATE questpoints SET questpoints = ? WHERE player = ?";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, questpoints);
            ps.setString(2, playername);
            ps.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}


