package com.zenya.sgcraftaddon.utilities;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoreUtils {
    private static LoreUtils instance;

    // Key: Data
    private HashMap<String, String> getData(ItemStack item) {
        HashMap<String, String> loreData = new HashMap<String, String>();

        if(item == null || item.getType().equals(Material.AIR)) {
            return loreData;
        }

        ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore();

        if(lores == null || lores.size() == 0) {
            return loreData;
        }

        for(String lore : lores) {
            if(!(lore.contains(": "))) {
                continue;
            }
            String[] rawData = lore.split(": ", 2);
            try {
                loreData.put(rawData[0], rawData[1]);
            } catch(Exception e) {
                continue;
            }
        }
        return loreData;
    }

    private ItemStack setData(ItemStack item, HashMap<String, String> data) {
        clearData(item);

        if(item == null || item.getType().equals(Material.AIR)) {
            return item;
        }

        ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore();

        if(lores == null || lores.size() == 0) {
            lores = new ArrayList<String>();
        }

        for(Map.Entry<String, String> entry : data.entrySet()) {
            lores.add(entry.getKey() + ": " + entry.getValue());
        }

        meta.setLore(lores);
        item.setItemMeta(meta);

        return item;
    }

    private ItemStack clearData(ItemStack item) {
        ArrayList<String> loresToRemove = new ArrayList<String>();

        if(item == null || item.getType().equals(Material.AIR)) {
            return item;
        }

        ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore();

        if(lores == null || lores.size() == 0) {
            return item;
        }

        for(String lore : lores) {
            if(!(lore.contains(": "))) {
                continue;
            }
            loresToRemove.add(lore);
        }

        for(String toRemove : loresToRemove) {
            lores.remove(toRemove);
        }

        meta.setLore(lores);
        item.setItemMeta(meta);

        return item;
    }

    public ArrayList<String> getKeys(ItemStack item) {
        HashMap<String, String> loreData = getData(item);
        ArrayList<String> keys = new ArrayList<String>();

        for(Map.Entry<String, String> entry : loreData.entrySet()) {
            keys.add(entry.getKey());
        }
        return keys;
    }

    public String getKey(ItemStack item, String key) {
        key = ChatColor.translateAlternateColorCodes('&', key);

        String value = getData(item).get(key);
        return value;
    }

    public ItemStack setKey(ItemStack item, String key, String value) {
        HashMap<String, String> loreData = getData(item);

        key = ChatColor.translateAlternateColorCodes('&', key);
        value = ChatColor.translateAlternateColorCodes('&', value);

        loreData.put(key, value);
        return setData(item, loreData);
    }

    public ItemStack removeKey(ItemStack item, String key) {
        HashMap<String, String> loreData = getData(item);

        key = ChatColor.translateAlternateColorCodes('&', key);

        try {
            loreData.remove(key);
        } catch(Exception e) {
            // It's ok, do nothing
        }
        return setData(item, loreData);
    }

    public ItemStack addLore(ItemStack item, String str) {
        if(item == null || item.getType().equals(Material.AIR)) {
            return item;
        }

        ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore();

        if(lores == null || lores.size() == 0) {
            lores = new ArrayList<String>();
        }

        lores.add(str);
        meta.setLore(lores);
        item.setItemMeta(meta);

        return item;
    }

    public ItemStack removeLore(ItemStack item, String str) {
        if(item == null || item.getType().equals(Material.AIR)) {
            return item;
        }

        ItemMeta meta = item.getItemMeta();
        List<String> lores = meta.getLore();

        if(lores == null || lores.size() == 0) {
            return item;
        }

        String loreToRemove = null;

        for (String lore : lores) {
            if (lore.contains(str)) {
                loreToRemove = lore;
            }
        }

        if (!(loreToRemove == null)) {
            lores.remove(loreToRemove);
            meta.setLore(lores);
            item.setItemMeta(meta);
        }
        return item;
    }

    public static LoreUtils getInstance() {
        if(instance == null) {
            instance = new LoreUtils();
        }
        return instance;
    }
}
