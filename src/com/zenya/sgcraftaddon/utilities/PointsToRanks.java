package com.zenya.sgcraftaddon.utilities;

import com.zenya.sgcraftaddon.main.SGCraftAddon;

import java.util.ArrayList;

public class PointsToRanks {
    ConfigManager configManager;

    public PointsToRanks() {
        configManager = SGCraftAddon.configManager;
    }

    public String convert(Integer questpoints) {
        ArrayList<String> ranks = new ArrayList<String>();
        String myRank;
        Integer rankQP;

        ranks = configManager.getRanks();

        for(int i=0; i<ranks.size(); i++) {
            if(i == 0) {
                myRank = ranks.get(0);
            } else {
                myRank = ranks.get(i-1);
            }

            rankQP = configManager.getRankQP(ranks.get(i));

            if(rankQP > questpoints) {
                return myRank;
            }
        }
        return ranks.get(ranks.size()-1);
    }
}
