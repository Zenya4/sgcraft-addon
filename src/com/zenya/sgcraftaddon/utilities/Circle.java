package com.zenya.sgcraftaddon.utilities;

import java.util.ArrayList;

public class Circle {
    private ArrayList<Double[]> outline;
    private ArrayList<Double[]> circle;
    private Integer[] centre;
    private Integer radius;
    private Double increment;

    public Circle(Integer[] centre, Integer radius, Double increment) {
        this.centre = centre;
        this.radius = radius;
        this.increment = increment;

    }

    public ArrayList<Double[]> generateCircle() {
        ArrayList<Double[]> circle = new ArrayList<Double[]>();
        for(double x=-radius; x < radius; x += increment) {
            for(double y=-radius; y < radius; y += increment) {
                if(isInside(x, y)) {
                    circle.add(new Double[]{x+centre[0], y+centre[1]});
                }
            }
        }
        this.circle = circle;
        return circle;
    }

    public ArrayList<Double[]> generateOutline() {
        ArrayList<Double[]> outline = new ArrayList<Double[]>();
        for(float angle = 0f; angle < 360f; angle += increment) {
            double x = radius * Math.sin(angle);
            double y = radius * Math.cos(angle);
            outline.add(new Double[]{x+centre[0], y+centre[1]});
        }
        this.outline = outline;
        return outline;
    }

    public Integer[] getCentre() {
        return centre;
    }

    public Integer getRadius() {
        return radius;
    }

    public Double getIncrement() {
        return increment;
    }

    public ArrayList<Double[]> getCircle() {
        return outline;
    }

    public ArrayList<Double[]> getOutline() {
        return outline;
    }

    private boolean isInside(Double x, Double y) {
        return x*x + y*y <= radius*radius;
    }
}
