package com.zenya.sgcraftaddon.utilities;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ChatUtils {

    public static String translateColor(String message) {
        message = ChatColor.translateAlternateColorCodes('&', message);
        return message;
    }

    public static void sendMessage(Player player, String message) {
        message = translateColor(message);
        player.sendMessage(message);
    }

    public static void sendMessage(CommandSender sender, String message) {
        message = translateColor(message);
        sender.sendMessage(message);
    }

    public static void sendBroadcast(String message) {
        message = translateColor(message);

        for(Player player : Bukkit.getServer().getOnlinePlayers()) {
            player.sendMessage(message);
        }
    }

    public static void sendProtectedBroadcast(List<String> permissions, String message) {
        message = translateColor(message);

        for(Player player : Bukkit.getServer().getOnlinePlayers()) {
            for(String permission : permissions) {
                if(player.hasPermission(permission)) {
                    player.sendMessage(message);
                    continue;
                }
            }
        }
    }
}
