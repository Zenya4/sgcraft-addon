package com.zenya.sgcraftaddon.interfaces;

public interface CItemTask {
    int health = 0;

    public void runTask();
    public int getHealth();
    public void reduceHealth();
}
