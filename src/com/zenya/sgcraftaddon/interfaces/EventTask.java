package com.zenya.sgcraftaddon.interfaces;

public interface EventTask {

    public void runTasks();
    public void updateData();
}
