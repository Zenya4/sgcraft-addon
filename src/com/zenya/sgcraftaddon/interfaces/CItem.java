package com.zenya.sgcraftaddon.interfaces;

import com.zenya.sgcraftaddon.storage.Trigger;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public interface CItem {

    public void onTrigger(Event event, Trigger trigger);
    public void onPassive(Player player); // Unused

    public String getKey();
    public List<Trigger> getTriggers();
    public List<String> getPermissions();
    public String getName();
    public List<String> getDescription();
    public Material getMaterial();
    public ItemStack getItem();
}
