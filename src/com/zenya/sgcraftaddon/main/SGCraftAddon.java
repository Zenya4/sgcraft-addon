package com.zenya.sgcraftaddon.main;

import com.zenya.sgcraftaddon.commands.*;
import com.zenya.sgcraftaddon.events.CItemListeners;
import com.zenya.sgcraftaddon.events.Listeners;
import com.zenya.sgcraftaddon.utilities.ConfigManager;
import com.zenya.sgcraftaddon.utilities.SQLiteManager;
import org.bukkit.plugin.java.JavaPlugin;

public class SGCraftAddon extends JavaPlugin {
    public static SGCraftAddon instance;
    public static SQLiteManager sqLiteManager;
    public static ConfigManager configManager;

    @Override
    public void onEnable() {
        instance = this;
        configManager = new ConfigManager(this);
        sqLiteManager = new SQLiteManager(this);

        this.getCommand("questpoints").setExecutor(new QuestPoints());
        this.getCommand("questranks").setExecutor(new QuestRanks());
        this.getCommand("maplock").setExecutor(new MapLock());
        this.getCommand("toolbox").setExecutor(new ToolBox());
        this.getCommand("events").setExecutor(new Events());
        this.getCommand("claimpvp").setExecutor(new ClaimPvP());

        this.getServer().getPluginManager().registerEvents(new Listeners(), this);
        this.getServer().getPluginManager().registerEvents(new CItemListeners(), this);
    }

    @Override
    public void onDisable() {

    }
}
