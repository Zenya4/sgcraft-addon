package com.zenya.sgcraftaddon.storage;

import me.ryanhamshire.GriefPrevention.Claim;

import java.util.HashMap;
import java.util.UUID;

public class ClaimPvPStorage {

    private static ClaimPvPStorage instance;
    private HashMap<Claim, Boolean> ispvp = new HashMap<Claim, Boolean>();
    private HashMap<UUID, Long> cooldown = new HashMap<UUID, Long>();

    public static ClaimPvPStorage getInstance() {
        if (instance == null) {
            instance = new ClaimPvPStorage();
        }
        return instance;
    }
    public boolean isPvpEnabled(Claim claim) {
        if (claim == null) {
            return false;
        }
        Boolean result = ispvp.get(claim);
        if (result == null) {
            setPVP(claim, false);
            return false;
        }
        return result;
    }
    public void setPVP(Claim claim, Boolean state) {
        ispvp.put(claim, state);
    }
    public Long getRawCooldown(UUID uuid) {
        return cooldown.get(uuid);
    }
    public int getCooldown(UUID uuid) {
        Long ccooldown = cooldown.get(uuid);
        int sec = (int) (ccooldown / 1000);
        return sec;
    }
    public void setCooldown(UUID uuid, Long ctime) {
        cooldown.put(uuid, ctime);
    }

}
