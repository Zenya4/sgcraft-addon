package com.zenya.sgcraftaddon.storage;

public enum Trigger {
    SNOWBALL_LAUNCH,
    SNOWBALL_HIT,
    ENTITY_SHOOT_BOW,
    PLAYER_INTERACT,
    PLAYER_INTERACT_AT_ENTITY,
    ENTITY_DAMAGE_BY_ENTITY,
    BLOCK_PLACE,
    PLAYER_MOVE,
    CONSUME_ITEM
}
