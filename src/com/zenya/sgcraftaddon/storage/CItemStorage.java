package com.zenya.sgcraftaddon.storage;

import com.zenya.sgcraftaddon.interfaces.CItem;
import com.zenya.sgcraftaddon.items.*;

import java.util.HashMap;
import java.util.Set;

public class CItemStorage {

    private static CItemStorage instance;
    private HashMap<String, CItem> customItems = new HashMap<String, CItem>();

    private CItemStorage() {
        registerItem("Firecracker", new Firecracker());
        registerItem("Minigun", new Minigun());
        registerItem("YeetStick", new YeetStick());
        registerItem("BanHammer", new BanHammer());
        registerItem("FireworkBattery", new FireworkBattery());
        registerItem("Flamethrower", new Flamethrower());
        registerItem("HookshotBow", new HookshotBow());
        registerItem("RocketBoots", new RocketBoots());
        registerItem("SnowBoots", new SnowBoots());
        registerItem("BoomStick", new BoomStick());
        registerItem("LightningRod", new LightningRod());
        registerItem("HiveWand", new HiveWand());
        registerItem("MagicChorus", new MagicChorus());
        registerItem("HomingBow", new HomingBow());
        registerItem("Egg", new Egg());
        registerItem("RodOfSouls", new RodOfSouls());
        registerItem("MagicBlox", new MagicBlox());

    }

    public Set<String> getKeys() {
        return customItems.keySet();
    }

    public void registerItem(String key, CItem item) {
        customItems.put(key, item);
    }

    public CItem getCItem(String key) {
        return customItems.get(key);
    }

    public static CItemStorage getInstance() {
        if(instance == null) {
            instance = new CItemStorage();
        }
        return instance;
    }
}
