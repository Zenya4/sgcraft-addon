package com.zenya.sgcraftaddon.storage;

public class ValueStorage {

    private static ValueStorage instance;

    public static boolean betterDragon = false;
    public static boolean letItSnow = false;
    public static boolean snowballBoom = false;

    public static ValueStorage getInstance() {
        if(instance == null) {
            instance = new ValueStorage();
        }

        return instance;
    }
}
