1.0.0:
- Initial release
- Added questpoints with SQLite DB

1.1.0:
- Added `/questpoints addall` subcommand
- Added questranks
- Added `sgcraftaddon.player` and `sgcraftaddon.admin` permission nodes
- Allowed players to pay respect [F]

1.2.0:
- Internal code cleanup
- Added map-piracy prevention `/maplock`

1.3.0:
- First test build of custom items `/toolbox`
- Added events "better dragon", "let it snow" `/events`
- Fixed bug with respect-paying not cancelling chat event

1.4.0:
- Made EventTasks synchronous to account for TPS lag
- Rewrote LoreUtils API
- Added a CItem `Firecracker`
- Added GUI for `/toolbox`
- Added Event `Snowball Boom`

1.5.0:
- Added Discord JDA
- Allowed plugin to auto-generate punishment reports in discord
- Added new CItems `Minigun`, `Yeet Stick` and `Ban Hammer`

1.6.0:
- Added new CItems `Rocket Boots`, `Firework Battery`, `Flamethrower`, `Hookshot Bow` and `Snow Boots` 
- Removed discord auto-report due to negative reviews
- Made CItem `Minigun` respect Infinity enchantment

1.6.1:
- Added back discord auto-report
- Added feature to bypass autoreport with `-s` flag in command
- Changed embed from `Punished By` to `Executed By` to account for unbans

1.6.2:
- Hotfix to prevent arrow-duping with Miniguns when they have the Infinity enchantment

1.6.3:
- Added null check in HookshotBow to fix console spam

1.6.4:
- Fixed even more console spam errors

2.0.0:
- Update to support 1.15.x
- Partial rewrite of `CItem` to clean up code and reduce errors
- Fixed database entry duplication bug
- Performance improvements by making database init in `LoginEvent` async
- Made `/toolbox` remain open after obtaining an item

2.0.1:
- Added new CItems `BoomStick`, `HiveWand`, `LightningRod` and `MagicChorus`
- Changed CItem `Minigun` to a crossbow to nerf bow-spamming
- Added a command `/toolbox give` to give CItems to players

2.0.2:
- Fixed a bug with `/maplock` being bypassable with cartography tables
- Fixed a bug with eating `MagicChorus` causing the entire stack to disappear
- Fixed a bug with `HiveWand` spawning too many bees
- Fixed a bug with CItems potentially working in pvp-disabled regions

2.0.3:
- Fixed a critical duplication bug in `/toolbox`

2.0.4:
- Another patch for bug fixed in `2.0.3`
- Added notification to staff when player trades with cartographer

2.1.0:
- Another patch (2) for bug fixed in `2.0.3`
- Added `Tornado` event

2.2.0:
- Added `/claimpvp` command to toggle PvP in GriefPrevention claims
- Fixed NPE in `InventoryClickEvent`

2.2.1:
- Removed `PayRespectsEvent` class
- Added "F", "X" and "Y" message triggers in `AsyncChatEvent`

2.2.2:
- Removed notification for when player trades with a Cartographer villager

2.2.3:
- Fixed NPE in `InventoryClickEvent`

2.2.4:
- Added new CItem `HomingBow`
- Fixed bug where `MagicChorus` does not deplete when consumed

2.2.5:
- Made `HomingBow` no longer target invisible players
- Made `HomingBow` only home after 2 ticks
- Fixed `HomingBow` permissions

2.2.5:
- Added CItem `Egg`

2.2.6:
- Added new CItem `RodOfSouls`
- Fixed bug where CItem `Egg` would duplicate forever and crash the server
- Fixed a bug where CItem `Egg` would get stuck in blocks
- Fixed a bug where CItem `MagicChorus` was not being depleted when consumed
- Fixed some casting errors (spam) in console

2.2.7:
- Added new CItem `MagicBlox`
- Fixed CItem `RodOfSouls` spawning particles in random directions
- Removed Discord hook (again)
- Compiled with 1.16.x

2.2.8:
- Added command pre-process interceptor to replace `%all%` with usernames of all players